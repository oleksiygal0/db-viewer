from PySide2 import QtCore, QtGui, QtWidgets
from PySide2.QtSql import QSqlTableModel, QSqlDatabase, QSqlQuery
from ImprovedFilter import multicolumnFilter, HorizontalHeader


class CoppyTable(QtWidgets.QDialog):
    def __init__(self, db, table_name):
        super(CoppyTable,self).__init__()
        self.table_n = table_name.lower()
        self.setupUi(self)
        self.model = QSqlTableModel(None, QSqlDatabase.cloneDatabase(db, 'coppy_dialog_conn'))
        self.model.database().open()
        self.init_table(table_name)
        self.init_combobox()
        
        

    def init_table(self, table_name):
        self.model.setTable(table_name)
        self.model.select()
        if self.model.lastError().isValid():
            self.model.database().close()
            self.model.database().open()
            self.model.select()
        print(self.model.lastError())
        proxy_filter = multicolumnFilter()
        proxy_filter.setSourceModel(self.model)
        header = HorizontalHeader(proxy_filter)
        self.tableView.setModel(proxy_filter)
        self.tableView.setHorizontalHeader(header)

    def reinit_model(self):
        self.model.setTable(self.comboBox.currentText())
        self.model.select()
        if self.model.lastError().isValid():
            self.model.database().close()
            self.model.database().open()
            self.model.select()
        print(self.model.lastError())
        proxy_filter = multicolumnFilter()
        proxy_filter.setSourceModel(self.model)
        self.tableView.horizontalHeader().set_model(proxy_filter)
        self.tableView.horizontalHeader().filter_init()
        self.tableView.setModel(proxy_filter)

    def init_combobox(self):
        query = QSqlQuery(self.model.database())
        query.prepare('SHOW TABLES;')
        tables = []
        query.exec_()
        while query.next(): 
            table_n=( query.value(0) )
            tables.append(table_n)
        table = (self.table_n[:self.table_n.find('_')]).lower()
        query.exec_('select * from '+table+';')
        values = []
        name_pattern = ''
        while query.next():
            values.append( (table+'_'+query.value(1)+'_'+query.value(2)).lower() )
        for value in values:
            if value in self.table_n:
                name_pattern = value
                break
        print(name_pattern)
        for t in tables:
            if name_pattern in t:
                self.comboBox.addItem(t)

    def construct_query(self):
        str_buffer = ''
        select_fields = ''
        self.tableView.model()
        for column in range( self.tableView.model().columnCount() ):
            if column==0:
                pass
            else:
                header_name = self.model.headerData(column, QtCore.Qt.Horizontal, QtCore.Qt.DisplayRole)
                if column < self.tableView.model().columnCount()-1:
                    select_fields = select_fields+str(header_name)+', '
                else: 
                    select_fields = select_fields+str(header_name)
        for row in range( self.tableView.model().rowCount() ):
            # if not self.tableView.isRowHidden(row):
            index = self.tableView.model().index(row, 0)
            id_value = self.tableView.model().data(index, QtCore.Qt.DisplayRole)
            header_name = self.model.headerData(0, QtCore.Qt.Horizontal, QtCore.Qt.DisplayRole)
            query = 'INSERT IGNORE INTO '+self.table_n+' ('+select_fields+')'+' SELECT '+select_fields+' FROM '+self.comboBox.currentText()+' WHERE '+str(header_name)+'='+str(id_value)+';\n'
            str_buffer = str_buffer + query
        self.textEdit.clear()
        self.textEdit.setText(str_buffer)

    def accept_action(self):
        str_buff = self.textEdit.toPlainText()
        if len(str_buff) >0:
            strbuff = []
            index = 0
            while True:
                temps = str_buff[index:str_buff.find('\n', index)]
                if len(temps)>0:
                    strbuff.append( temps)
                index = str_buff.find('\n', index, len(str_buff))+1
                if index == 0:
                    break
            self.model.database().close()
            self.model.database().open()
            query = QSqlQuery(self.model.database())
            print( query.driver() )
            for i in strbuff:
                query.prepare(i)
                # print(i)
                query.exec_()
                if query.lastError().isValid():
                    self.plainTextEdit.setPlainText( query.lastError().text() )
                    if 'MySQL server has gone away' in query.lastError().text():
                        query.driver().close()
                        query.driver().open()
                        query.prepare(i)
                        query.exec_()
                    else: 
                        return None
            self.Dialog.accept()
        else:
            self.plainTextEdit.setPlainText('Please generate query')


    def setupUi(self, Dialog):
        self.Dialog = Dialog
        self.Dialog.setObjectName("Dialog")
        self.Dialog.resize(1104, 903)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.Dialog)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtWidgets.QLabel(self.Dialog)
        font = QtGui.QFont()
        font.setFamily("Calibri")
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        spacerItem = QtWidgets.QSpacerItem(158, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.comboBox = QtWidgets.QComboBox(self.Dialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.comboBox.sizePolicy().hasHeightForWidth())
        self.comboBox.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Calibri")
        self.comboBox.setFont(font)
        self.comboBox.setObjectName("comboBox")
        self.horizontalLayout.addWidget(self.comboBox)
        self.verticalLayout_2.addLayout(self.horizontalLayout)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.label_2 = QtWidgets.QLabel(self.Dialog)
        font = QtGui.QFont()
        font.setFamily("Calibri")
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.verticalLayout.addWidget(self.label_2)
        self.tableView = QtWidgets.QTableView(self.Dialog)
        font = QtGui.QFont()
        font.setFamily("Calibri")
        self.tableView.setFont(font)
        self.tableView.setObjectName("tableView")
        self.verticalLayout.addWidget(self.tableView)
        self.verticalLayout_2.addLayout(self.verticalLayout)
        self.label_3 = QtWidgets.QLabel(self.Dialog)
        font = QtGui.QFont()
        font.setFamily("Calibri")
        self.label_3.setFont(font)
        self.label_3.setObjectName("label_3")
        self.verticalLayout_2.addWidget(self.label_3)
        self.pushButton = QtWidgets.QPushButton(self.Dialog) ####
        self.pushButton.setObjectName("pushButton")     ####
        self.verticalLayout_2.addWidget(self.pushButton)####
        self.textEdit = QtWidgets.QTextEdit(self.Dialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.textEdit.sizePolicy().hasHeightForWidth())
        self.textEdit.setSizePolicy(sizePolicy)
        self.textEdit.setMaximumSize(QtCore.QSize(16777215, 80))
        font = QtGui.QFont()
        font.setFamily("Calibri")
        self.textEdit.setFont(font)
        self.textEdit.setObjectName("textEdit")
        self.verticalLayout_2.addWidget(self.textEdit)
        self.label_4 = QtWidgets.QLabel(self.Dialog)
        font = QtGui.QFont()
        font.setFamily("Calibri")
        self.label_4.setFont(font)
        self.label_4.setObjectName("label_4")
        self.verticalLayout_2.addWidget(self.label_4)
        self.plainTextEdit = QtWidgets.QPlainTextEdit(self.Dialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.plainTextEdit.sizePolicy().hasHeightForWidth())
        self.plainTextEdit.setSizePolicy(sizePolicy)
        self.plainTextEdit.setMinimumSize(QtCore.QSize(40, 60))
        self.plainTextEdit.setMaximumSize(QtCore.QSize(16777215, 70))
        font = QtGui.QFont()
        font.setFamily("Calibri")
        self.plainTextEdit.setFont(font)
        self.plainTextEdit.setObjectName("plainTextEdit")
        self.verticalLayout_2.addWidget(self.plainTextEdit)
        self.buttonBox = QtWidgets.QDialogButtonBox(self.Dialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout_2.addWidget(self.buttonBox)

        self.retranslateUi()
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("accepted()"), self.accept_action)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("rejected()"), self.Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(self.Dialog)
        self.comboBox.currentIndexChanged.connect(self.reinit_model)
        self.pushButton.clicked.connect(self.construct_query)

    def retranslateUi(self):
        self.Dialog.setWindowTitle(QtWidgets.QApplication.translate("Dialog", "Dialog", None, -1))
        self.label.setText(QtWidgets.QApplication.translate("Dialog", "Select Table", None, -1))
        self.label_2.setText(QtWidgets.QApplication.translate("Dialog", "Select data for coppy.", None, -1))
        self.label_3.setText(QtWidgets.QApplication.translate("Dialog", "SQL Query", None, -1))
        self.pushButton.setText(QtWidgets.QApplication.translate("Dialog", "Generate Query", None, -1))
        self.label_4.setText(QtWidgets.QApplication.translate("Dialog", "SQL Error", None, -1))


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# DEBUG PART
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if __name__=='__main__':
    import sys
    from PySide2.QtWidgets import QApplication
    app = QtWidgets.QApplication(sys.argv)
    
    import configparser         # !!!! try to figure out why sql connection not initialise with literals!
    conf = configparser.ConfigParser()
    conf.read('config.ini')
    user = conf['connection']['user']
    password = conf['connection']['password']
    host = conf['connection']['host']
    database = conf['connection']['database']
    dsn = conf['connection']['DSN']
    port = conf['connection']['port']

    db = QSqlDatabase.addDatabase("QMYSQL", 'MyConn')
    db.setHostName(host)
    db.setDatabaseName(database)
    db.setUserName(user)
    db.setPassword(password)
    db.setPort(int(port))

    window = CoppyTable(db, 'slg5nt1769_rds_on_zs_1')
    window.show()

    sys.exit(app.exec_())