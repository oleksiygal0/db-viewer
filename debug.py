# import configparser         # !!!! try to figure out why sql connection not initialise with literals!
# conf = configparser.ConfigParser()
# conf.read('config.ini')
# user = conf['connection']['user']
# password = conf['connection']['password']
# host = conf['connection']['host']
# database = conf['connection']['database']
# dsn = conf['connection']['DSN']
# port = conf['connection']['port']

# import mysql.connector as sql
# from pandas import DataFrame
# import pandas as pd
# import numpy as np
# from matplotlib import pyplot as plt
# import matplotlib
# from matplotlib.widgets import CheckButtons
# matplotlib.use('Qt5Agg') # <-- THIS MAKES IT FAST!

# db_connection = sql.connect(host=host, database=database, user = user, password = password, port = port, auth_plugin='mysql_native_password')
# df = pd.read_sql('SELECT * FROM slg5aph1320_acl_xd_0;',db_connection)

# #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# # PIVOT PLOT
# #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# idx = ['TA','VD_V_arg','RSET_Ohm_arg']

# TA = [85.0]
# d = df[df['TA'].isin(TA)]   #  DATA FILTERING
# rset = [62.0]
# d = d[d.RSET_Ohm_arg.isin(rset)]

# pt = df.pivot_table(columns = 'Chip', values = 'ACL_A_res', index = idx)
# pt = pt[pt['TA'].isin(TA)]
# ax = pt.plot.line() #get axes object , initialize line plot

# fig  = ax.get_figure()  # get figure object

# leg = ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), shadow=True, ncol=20) 
# # associate lines object with legend lines
# lines = ax.get_lines()
# lined = dict()
# for legline, origline in zip(leg.get_lines(), lines):
#     legline.set_picker(5)  # 5 pts tolerance
#     lined[legline] = origline

# def onpick(event):
#     # on the pick event, find the orig line corresponding to the
#     # legend proxy line, and toggle the visibility
#     legline = event.artist
#     origline = lined[legline]
#     vis = not origline.get_visible()
#     origline.set_visible(vis)
#     # Change the alpha on the line in the legend so we can see what lines
#     # have been toggled
#     if vis:
#         legline.set_alpha(1.0)
#     else:
#         legline.set_alpha(0.2)
#     fig.canvas.draw()
# fig.canvas.mpl_connect('pick_event', onpick)

# plt.show()

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# STATISTIC PLOT
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# idx = ['VD_V_arg','RSET_Ohm_arg']
# # pt = df.pivot_table(columns = 'Chip', values = 'ACL_A_res', index = idx)
# pt = df.pivot_table(values = 'ACL_A_res', index = idx, aggfunc=[np.mean, np.median, min, max])
# pt.plot()
# plt.show()

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# BOX PLOT
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# df = df.loc[df['TA']==85.0]
# idx = ['writer','VD_V_arg','RSET_Ohm_arg']
# ax = df.boxplot(column='ACL_A_res', by = idx, rot = 90)
# # ax.set
# plt.show()

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# HISTOGRAM PLOT
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# df = df.loc[df['TA']==85.0]
# idx = ['VD_V_arg','RSET_Ohm_arg']
# ax = df.hist(column='ACL_A_res', by = idx, bins = 15)
# plt.show()

#################################################################################################################
import sys
from PySide2 import QtCore, QtWidgets

# class MyDelegate(QtWidgets.QItemDelegate):
#     comboItems=['Combo_Zero', 'Combo_One','Combo_Two']
#     def createEditor(self, parent, option, proxyModelIndex):
        
#         combo = QtWidgets.QComboBox(parent)
#         combo.addItems(self.comboItems)
#         # combo.setEditable(True)
#         self.connect(combo, SIGNAL("currentIndexChanged(int)"), self, SLOT("currentIndexChanged()"))
#         return combo

#     def setModelData(self, combo, model, index):
#         comboIndex=combo.currentIndex()
#         text=self.comboItems[comboIndex]        
#         model.setData(index, text)
#         print '\t\t\t ...setModelData() 1', text

#     @pyqtSlot()
#     def currentIndexChanged(self): 
#         self.commitData.emit(self.sender())
# class MyCustomWidget(QtWidgets.QWidget):
#     def __init__(self, name, parent=None):
#         super(MyCustomWidget, self).__init__(parent)
#         self.row = QtWidgets.QHBoxLayout()
#         lineEdit = QtWidgets.QLineEdit()
#         lineEdit.setText(name)
#         self.row.addWidget(lineEdit)
#         combo = QtWidgets.QComboBox()
#         combo.addItem('one')
#         combo.addItem('two')
#         self.row.addWidget(combo)
#         self.setLayout(self.row)


# if __name__ == '__main__':

#     app = QtWidgets.QApplication(sys.argv)
#     view = QtWidgets.QListWidget()
#     item = QtWidgets.QListWidgetItem()
#     view.addItem(item)

#     myWidg = MyCustomWidget('OHA')
    
#     item.setSizeHint(myWidg.minimumSizeHint())
#     view.setItemWidget(item, myWidg)
#     view.show()
    
#     sys.exit(app.exec_())

