import configparser 
import mysql.connector as sql
import pandas as pd

conf = configparser.ConfigParser()
conf.read('config.ini')
user = conf['connection']['user']
password = conf['connection']['password']
host = conf['connection']['host']
database = conf['connection']['database']
port = conf['connection']['port']
db_connection = sql.connect(host=host, database=database, user = user, password = password, 
    port = port, auth_plugin='mysql_native_password')
query = 'SELECT * FROM slg5aph1320_idd_off_xd_0;'
df = pd.read_sql(query,db_connection)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# START WORK WITH DATAFRAME

# DROP COLUMNS
column_to_adrop = ['mesure_id','Channel','VDD_V_arg','ON_OFF', 'Product', 'f_key', 'Outlier', 'writer']

edited_df = df.drop(column_to_adrop, axis = 1)      # deflete specified columns from DF 

# REORDER COLUMNS
col = ['Chip', 'VD_V_arg', 'OnVoltage_V', 'IDD_A_res', 'TA', 'Timestamp']   # new columns order
edited_df = edited_df[col]                          # reorder columns in DF

# RENAME COLUMNS
edited_df = edited_df.rename(columns={'VD_V_arg':'VD(V)', 'OnVoltage_V':'OnVoltage(V)', 'IDD_A_res':'IDD(A)'}, errors="raise")

# SORTING COLUMNS
sorting_lilt = ['TA', 'Chip', 'VD(V)']
# for i in sorting_lilt:
edited_df = edited_df.sort_values(by = sorting_lilt)

# EXPORT TO EXCEL
edited_df.to_excel('C:\\Users\\ohal\\dev\\db_view\\IDD_OFF_Rev_XD_Different_Temperatures_1.xlsx', sheet_name='RawData', index = False)
# END WORK WITH DATAFRAME
# #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
CALL C# DINAMIC LIBLARY
import sys
import clr

assembly_path = r"C:\\Users\\ohal\source\\repos\\PVT_DLL\\PVT_DLL\\bin\\Debug"

sys.path.append(assembly_path)  # add dll directory to python path

clr.AddReference("PVT_DLL") # import dll, note that ".dll" extention is missed

from PVT_DLL import Class1   # from namespace "Calculation" import class "calculate"

pvt = Class1()

pvt.createTable()




