'''
THIS MODULE IMPLEMENT CUSTOM HEADER VIEW AND CUSTOM FILTER MODEL.
FOR PROPPER MODULE USAGE YOU SHOULD PERFORM NEXT STEPS:
1. Implement create table model;
2. Make all set-up`s for your table model;
3. Create multicolumnFilter() instance: 
            my_filter = multicolumnFilter()
4. Transmit your model into multicolumnFilter instance with setSourceModel method:
            my_filter.setSourceModel(my_table_model)
5. Create HorizontalHeader() instance, consider that constructor oh HorizontalHeader 
   require proxy filter model:
            header = HorizontalHeader(my_filter)
6. Create view for your table model, table view constructor require table model, 
   use filter model as argument:
            my_view = QTableView(my_filter)
7. Transmit header instance into table view:
            my_view.setHorizontalHeader(header)
'''

from PySide2.QtSql import QSqlDatabase, QSqlError, QSqlTableModel, QSqlQueryModel, QSqlQuery
from PySide2.QtCore import QCoreApplication, Qt, QPoint, QObject, QEvent, QDateTime
from PySide2 import QtCore
import sys, os
from PySide2.QtWidgets import QApplication, QTableView, QHeaderView, QComboBox, QToolButton, QWidget, QDialog, QStyledItemDelegate
from PySide2.QtGui import QCursor, QWindow, QStandardItemModel, QStandardItem
from FilterMenu import Ui_Dialog



class multicolumnFilter(QtCore.QSortFilterProxyModel):
    def __init__(self, *args, **kwargs):
        QtCore.QSortFilterProxyModel.__init__(self, *args, **kwargs)
        self.filter_columns = {}
        

    def set_filter_columns(self, filter_dict):
        self.filter_columns = filter_dict
        

    def yield_values(self, reg_exp, column_index):
        self.filter_columns[column_index]=reg_exp
        self.invalidateFilter()

    def filterAcceptsRow(self, source_row, source_parent):
        for key, reg_exp in self.filter_columns.items():
            model_index = self.sourceModel().index(source_row, key, source_parent)
            if model_index.isValid():
                text_data = str( self.sourceModel().data(model_index) )
                if text_data[len(text_data)-2:]=='.0':
                    text_data = text_data[:len(text_data)-2]
                if not text_data in reg_exp.pattern():
                    # print(text_data)
                    return False
        return True

class ExFilter(QDialog):
    '''
        THIS CLASS PROVIDE DIALOG WINDOW FOR DETERMINING DATA
        THAT SHOULD BE APPLIED TO FILTER PROXY MODEL
    '''
    def __init__(self, values):
        super(ExFilter,self).__init__()
        self.view = Ui_Dialog()
        self.view.setupUi(self)
        list_view = self.view.get_list()
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        #CREATE MODEL FOR LIST VIEW
        self.model = QStandardItemModel( list_view )
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # LIST self.items[QStandardItem()] CONTAINS WHALL DATA THAT FILTER LIST SHOULD CONTAIN
        self.items = []
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # LIST self.filtered_data[] CONTAINS DATA THAN SHOULD BE LEFT IN TABLE AFTER FILTER HAS BEEN APPLIED
        self.filtered_data = []
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # CYCLE CONSTRUCT LIST MODEL AND APPEND DATA TO THE MODEL
        for value in values:
            if type(value) == QDateTime:
                item = QStandardItem()
                item.setData(value.toString(Qt.TextDate), Qt.DisplayRole)
            else:
                item = QStandardItem()
                item.setData(value, Qt.EditRole)
            item.setCheckable(True)
            item.setCheckState(Qt.Checked)
            self.model.appendRow(item)
            self.items.append(item)
        list_view.setModel(self.model)
        QtCore.QObject.connect(self.view.buttonBox, QtCore.SIGNAL("accepted()"), self.accept_action)
        self.view.checkBox.stateChanged.connect(self.select_all)

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # SLOT THAT SHOULD RESPOND ON ACCEPT EVENT IN DIALOG BOX
    # HERE I REFILL filtered_data LIST WITH NEW VALUES
    def accept_action(self):
        self.filtered_data = []
        for item in self.items:
            if item.checkState() == Qt.Checked:
                self.filtered_data.append(item.data(Qt.DisplayRole))
        self.accept()

    def get_filtered_data(self):
        return self.filtered_data

    def select_all(self):
        state = self.view.checkBox.checkState()
        if state == Qt.Unchecked:
            for item in self.items:
                item.setCheckState(Qt.Unchecked)
        elif state == Qt.Checked:
            for item in self.items:
                item.setCheckState(Qt.Checked)

class myButton(QToolButton):
    '''
    CURRENT CLASS HAS REIMPLEMENTED JUST FOR PROPPER DETEMINATION OF CURSOR COORDINATES
    WHEN mousePressEvent APPEARS
    '''
    def __init__(self, parent=None):
        QToolButton.__init__(self, parent)
        self.point = QPoint()
        self.parent = parent

    def mousePressEvent(self, event):
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # CALL OVERLOADED METHOD mousePressEvent OF HorizontalHeader CLASS
        self.parent.mousePressEvent(event, 'i am a pretty button')
        super(myButton, self).mousePressEvent(event) 

class HorizontalHeader(QHeaderView):
    '''
    HorizontalHeader CLASS IMPLEMENTATION FOR HEDER CUSTOMISATION
    EACH COLUMN SHOULD CONTAIN FILTER BUTTON
    '''
    def __init__(self, model, parent=None):
        super(HorizontalHeader, self).__init__(Qt.Horizontal, parent)    
        self.setSectionsMovable(True)
        self.comboboxes = []
        self.sectionResized.connect(self.handleSectionResized)
        self.sectionMoved.connect(self.handleSectionMoved)
        self.model = model
        self.filterDialogs = []         # list of ExFilter instances
        # self.setSectionsClickable(True)
        self.filter_init()

    def filter_init(self):
        filter_dict = {}
        print(self.model.rowCount())
        for column in range(self.model.columnCount()):
            data = []
            for row in range(self.model.rowCount()):
                index = self.model.index(row, column)
                data.append( self.model.data(index) )
            data = set(data)
            data = list(data)
            data.sort()
            data_reg_exp = self.build_reg_exp(data)
            # self.model.yield_values(QtCore.QRegularExpression(data_reg_exp), column)
            filter_dict[column] = QtCore.QRegularExpression(data_reg_exp)
            filter_window = ExFilter(data)
            self.filterDialogs.append(filter_window)
        self.model.set_filter_columns(filter_dict)
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # START COMMENT_BLOCK_1
    '''
    METHODS: showEvent, handleSectionResized, handleSectionMoved, fixComboPositions -
    RESPONSE JUST FOR HEADER RENDERING AND FOR PROPPER RESISE 
    '''
    def showEvent(self, event):
        for i in range(self.count()):
            if i < len(self.comboboxes):
                combo = self.comboboxes[i]
            else:
                combo = myButton(self)
                self.comboboxes.append(combo)
            combo.setGeometry( self.sectionViewportPosition(i)+4, 0, 24, self.height() )
            combo.show()
        if len(self.comboboxes) > self.count():
            for i in range(self.count(), len(self.comboboxes)):
                self.comboboxes[i].deleteLater()
        super(HorizontalHeader, self).showEvent(event)

    def handleSectionResized(self, i):
        print('SectionResized')
        for i in range(self.count()):
            j = self.visualIndex(i)
            logical = self.logicalIndex(j)
            self.comboboxes[i].setGeometry(self.sectionViewportPosition(logical)+4, 0, 24, self.height())

    def handleSectionMoved(self, i, oldVisualIndex, newVisualIndex):
        print('SectionMoved')
        for i in range(min(oldVisualIndex, newVisualIndex), self.count()):
            logical = self.logicalIndex(i)
            self.comboboxes[i].setGeometry(self.sectionViewportPosition(logical)+4, 0, 24, self.height())

    def fixComboPositions(self):
        print('fixComboPositions')
        for i in range(self.count()):
            self.comboboxes[i].setGeometry(self.sectionViewportPosition(i)+4, 0, 24, self.height())
    
    # END COMMENT_BLOCK_1
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    def get_count(self, logical_index):
        self.filterDialogs[logical_index].exec_()
        if self.filterDialogs[logical_index].result()==1:
            sorted_list = self.filterDialogs[logical_index].get_filtered_data()
            print(sorted_list)
            fedexp = self.build_reg_exp(sorted_list)
            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            #APPLY FILTER 
            self.model.yield_values(QtCore.QRegularExpression(fedexp), logical_index )
            # self.model.filterAcceptsRow()
            self.model.setFilterRegularExpression( QtCore.QRegularExpression(fedexp) )
            self.model.setFilterKeyColumn(logical_index)

    def build_reg_exp(self, sorted_list):
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        #CONSTRUCT STRING FOR QRegularExpression
        fedexp = '('
        for i in range( len(sorted_list) ):
            nlz_str = str(sorted_list[i])
            if nlz_str[len(nlz_str)-2:]=='.0':
                nlz_str = nlz_str[:len(nlz_str)-2]
            if i == (len(sorted_list)-1):
                fedexp =fedexp+'^'+nlz_str+'$'
            else:
                fedexp = fedexp+'^'+nlz_str+'$'+'|'
        fedexp = fedexp+')'
        return fedexp
     
    def mousePressEvent(self, event, modifier = None):
        super(HorizontalHeader, self).mousePressEvent(event)
        if modifier=='i am a pretty button':
            p = event.windowPos()   # return QPointF object, coordinates of regard to current QT window
            p = p.toPoint()         # convert QPointF into QPoint
            self.get_count(self.logicalIndexAt(p))



if __name__ == "__main__":
    import configparser         # !!!! try to figure out why sql connection not initialise with literals!
    conf = configparser.ConfigParser()
    conf.read('config.ini')
    user = conf['connection']['user']
    password = conf['connection']['password']
    host = conf['connection']['host']
    database = conf['connection']['database']
    dsn = conf['connection']['DSN']
    port = conf['connection']['port']

    os.environ["QT_AUTO_SCREEN_SCALE_FACTOR"] = "1"
    app = QApplication(sys.argv)
    app.setAttribute(Qt.AA_EnableHighDpiScaling)
    db = QSqlDatabase.addDatabase("QMYSQL")
    db.setHostName(host)
    db.setDatabaseName(database)
    db.setUserName(user)
    db.setPassword(password)
    db.setPort(int(port))

    model = QSqlTableModel()
    model.setTable('slg5aph1320_acl_xd_0')
    model.select()
    # print(model.lastError())
    proxy_model = multicolumnFilter()
    proxy_model.setSourceModel(model)

    view = QTableView()

    view.setModel(proxy_model)
    header  = HorizontalHeader(proxy_model)
    view.setHorizontalHeader(header)
    # view.setSortingEnabled(True)
    view.show()
    sys.exit(app.exec_())

