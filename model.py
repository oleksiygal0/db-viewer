from PySide2.QtGui import QStandardItemModel, QStandardItem
from PySide2.QtSql import QSqlTableModel, QSqlDatabase, QSqlError, QSqlQuery, QSqlQueryModel
import configparser

class oha_model(QStandardItemModel):

    def __init__(self):
        super(oha_model,self).__init__()

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        #READ DB CREDENTIALS FROM INI FILE
        conf = configparser.ConfigParser()
        conf.read('config.ini')
        self.user = conf['connection']['user']
        self.password = conf['connection']['password']
        self.host = conf['connection']['host']
        self.database = conf['connection']['database']
        self.dsn = conf['connection']['DSN']
        self.port = conf['connection']['port']

        db = QSqlDatabase.addDatabase('QMYSQL', 'con1')     # DATABASE CONNECTION
        db.setHostName(self.host)
        db.setDatabaseName(self.database)
        db.setUserName(self.user)
        db.setPassword(self.password)
        db.setPort(int(self.port))
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        #ASSIGN VIEW OBJECT TO VARIABLE
        self.tree = self.createTreeModel(db)
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # CREATE TABLE MODEL
        self.table = self.create_Table_Model(db)
        self.table.database().open()
        # self.db.open
        # print(self.db)

    def create_Table_Model(self, db):
        return QSqlTableModel(None, QSqlDatabase.cloneDatabase(db, 'main_window_conn'))
        
    def refresh_tree(self):
        self.tree = self.createTreeModel(self.table.database())

    def get_db(self):
        return self.db

    def get_tree(self):
        return self.tree

    def get_table(self):
        return self.table

    def get_db_credential(self):
        return {'user':self.user, 'psw':self.password,'host':self.host,'db':self.database,'dsn':self.dsn,'port':int(self.port)}

    def createTreeModel(self, db):
        model = QStandardItemModel()
        
        model.setHorizontalHeaderLabels(['Test Name', 'Chip Revision', 'Test Plan №', 'Version'])
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # GET DATA FROM DATABASE
        # heap = self.get_data(self.db.database('con1',True))  # this function connect to mysql server and fetch table names heap
        heap = self.get_data(db)
        products = heap[0]  # [(id_1, product_name), ... (id_n, product_name)]
        chip = heap[1]      # [(id_1, chip_name_1, foreign_key), ... (id_n, chip_name_n, foreign_key)]   
        testDict = heap[2]  # {chip_1:[[test_1, rev, testPlan, Version],...[test_n, rev, testPlan]], chip_2:[[test_1, rev, testPlan],...[test_n, rev, testPlan]]}

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # FILL MODEL WITH DATA - DB METHODS SHOULD BE CALLED THERE
        for i in products:
            parent1 = QStandardItem( 'Family - {}'.format( i[1] ) )
            for j in chip:
                if j[2] == i[0]:
                    parent2 = QStandardItem( j[1] )
                    for t in testDict[ j[1] ]:      # just taka propper test list from dictionary
                        child1 = QStandardItem( t[0] )          # Test Name
                        child2 = QStandardItem( t[1] )          # Chip Revision
                        child3 = QStandardItem( str(t[2]) )     # Test Plan №
                        child4 = QStandardItem( t[3] )          # Test Version
                        parent2.appendRow([child1, child2, child3, child4])
                    parent1.appendRow(parent2)
            model.appendRow(parent1)
        return model

    def TablePreview (self, tablename):
        self.table.setTable(tablename)
        self.table.select()
        if self.table.lastError().isValid():
            print( self.table.lastError() )
            self.table.database().close()
            self.table.database().open()
            # self.db1.close()
            # self.db1.open()
            self.table.setTable(tablename)
            self.table.select()
            print( self.table.lastError() )


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # GET DATA FROM DB FOR TREE VIEW
    def get_data (self, db):
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # ESTABLISH CONNECTION WITH SERVER       
        db.open()
        query = QSqlQuery( db )
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # FETCH DATA FROM SERVER       
        try:
            query.exec_("select id, name from products;")   
            products = []           # [(id_1, product_name), ... (id_n, product_name)]
            while query.next(): 
                tu=( query.value(0),query.value(1) )
                products.append(tu)
            query.setForwardOnly(True)
            query.exec_("select id, ch_name, product from chip;")
            chip = []    # [(id_1, chip_name_1, foreign_key), ... (id_n, chip_name_n, foreign_key)]
            while query.next(): 
                tu=( query.value(0),query.value(1), query.value(2) )
                chip.append(tu)
            testDict = {a[1]:None for a in chip}    #{chip_1:[[test_1, rev, testPlan, version],...[test_n, rev, testPlan, version]], chip_2:[[test_1, rev, testPlan, version],...[test_n, rev, testPlan, version]]}
            for key, val in testDict.items():
                q = 'select * from ' + key + ';'
                query.exec_(q)
                test_list = []
                while query.next(): 
                    test_list.append( (query.value(1), query.value(2), query.value(5), query.value(6)) )
                tl = []
                for i in test_list:
                    q2 =  'SHOW TABLES LIKE '+'\''+key+'_'+i[0]+'_'+i[1]+'_'+str(i[2])+'\';'
                    query.exec_( q2 )
                    if query.next():
                        tl.append(i)
                if len(tl) > 0:
                    testDict[key] = tl
            return products, chip, testDict
        except query.lastError() as er:
            print(er)
            return None
        db.close()

if __name__ == "__main__":
    model = oha_model()
    model.createModel()


