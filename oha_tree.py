# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'oha_tree.ui',
# licensing of 'oha_tree.ui' applies.
#
# Created: Mon Jun  3 16:38:18 2019
#      by: pyside2-uic  running on PySide2 5.12.2
#
# WARNING! All changes made in this file will be lost!+

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1180, 758)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.OHA_frame = QtWidgets.QFrame(self.centralwidget)
        self.OHA_frame.setGeometry(QtCore.QRect(10, 40, 471, 631))
        self.OHA_frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.OHA_frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.OHA_frame.setObjectName("OHA_frame")
        self.OHA_treeView = QtWidgets.QTreeView(self.OHA_frame)
        self.OHA_treeView.setGeometry(QtCore.QRect(10, 30, 451, 591))
        self.OHA_treeView.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.OHA_treeView.setObjectName("OHA_treeView")
        self.treeLabe = QtWidgets.QLabel(self.OHA_frame)
        self.treeLabe.setGeometry(QtCore.QRect(10, 10, 121, 16))
        self.treeLabe.setObjectName("treeLabe")
        self.frame = QtWidgets.QFrame(self.centralwidget)
        self.frame.setGeometry(QtCore.QRect(490, 40, 661, 631))
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.tableView = QtWidgets.QTableView(self.frame)
        self.tableView.setGeometry(QtCore.QRect(10, 30, 641, 591))
        self.tableView.setObjectName("tableView")
        self.label_2 = QtWidgets.QLabel(self.frame)
        self.label_2.setGeometry(QtCore.QRect(10, 10, 91, 16))
        self.label_2.setObjectName("label_2")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1180, 22))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)


        # self.OHA_treeView.setContextMenuPolicy()
        
        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        
    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QtWidgets.QApplication.translate("MainWindow", "Database Viewer", None, -1))
        self.treeLabe.setText(QtWidgets.QApplication.translate("MainWindow", "DATABASE NAVIGATION", None, -1))
        self.label_2.setText(QtWidgets.QApplication.translate("MainWindow", "TABLE PREVIEW", None, -1))


    def setTreeModel(self, tree_model):
        self.OHA_treeView.setModel(tree_model)
        
    def setTableModel(self, table_model):
        self.tableView.setModel(table_model)

    def getTree(self):
        return self.OHA_treeView

    def getTable(self):
        return self.tableView

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # THIS FUNCTION JUST BUILD RIGHT TABLE NAME
    def get_table_name(self):
        tstr = ''
        for i in self.OHA_treeView.selectedIndexes():
            if len( i.data() )> 0:
                try: 
                    tstr = tstr +'_'+ i.data()
                except:
                    return None
        index = self.OHA_treeView.selectedIndexes()
        chip = index[0].parent().data() 
        tstr = chip+tstr
        # print(tstr)
        if " " in tstr:
            return None
        else:
            return tstr