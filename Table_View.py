'''
    WARNING ! : TABLE VIEW USE OLD FILTER PROXY MODEL  !!!!
'''
from PySide2.QtCore import Qt
from PySide2 import QtCore, QtGui, QtWidgets
from PySide2.QtWidgets import QApplication, QMainWindow, QDialog, QHeaderView
from create_table_dialog import Ui_CreateTable
from PySide2.QtSql import QSqlQuery, QSqlTableModel, QSqlDatabase
from ReportWizard import ReportWizard

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# IMPORT MODEL
from Table_Model import Test_t_model
from ImprovedFilter import multicolumnFilter, HorizontalHeader

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# DIALOG BOX CLASS
class create_table_dialog(QDialog):
    def __init__(self, table, db):
        super(create_table_dialog, self).__init__()
        self.view = Ui_CreateTable()
        self.t = table
        self.view.set_Test_Table_Name(self.t)
        self.view.set_db(db)
        self.view.setupUi(self)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# CLASS FOR IMPORT DATA FROM TABLE SIBLING
from CoppyTableDialog import CoppyTable

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# CLASS FOR PLOT CREATION
from PlotWizard import PlotWizard

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# MAIN WINDOW CLASS
class Table_Ui(QMainWindow):

    def __init__(self, MW_name, db):
        super(Table_Ui, self).__init__()
        self.table = MW_name
        self.setupUi(self, MW_name)
        self.model = Test_t_model(db, MW_name)
        self.model.model.setTable(MW_name)
        self.model.model.select()

        self.filter_proxi_model = multicolumnFilter()
        self.filter_proxi_model.setSourceModel(self.model.get_t_model())
        self.header = HorizontalHeader(self.filter_proxi_model)
        self.tableView.setModel(self.filter_proxi_model)
        self.tableView.setHorizontalHeader(self.header)
        self.model.model.setEditStrategy(QSqlTableModel.OnManualSubmit)
        # self.tableView.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeToContents)

        self.actionSave.triggered.connect(self.model.applyChanges)
        # self.actionSave.triggered.connect(self.header.filter_init)
        self.actionSave.triggered.connect(self.update_view)

    def update_view(self):
        self.filter_proxi_model = multicolumnFilter()
        self.filter_proxi_model.setSourceModel(self.model.get_t_model())
        self.tableView.horizontalHeader().set_model(self.filter_proxi_model)
        self.tableView.horizontalHeader().filter_init()
        self.tableView.setModel(self.filter_proxi_model)

    def setupUi(self, MainWindow, MW_name):
        
        self.table_n = MW_name
        MainWindow = MainWindow
        MainWindow.setObjectName("Table Analisis")
        MainWindow.resize(1266, 909)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.tableView = QtWidgets.QTableView(self.centralwidget)
        # self.tableView.setGridStyle(QtCore.Qt.NoPen)
        self.tableView.setObjectName("tableView")
        self.gridLayout.addWidget(self.tableView, 0, 0, 1, 1)
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton.sizePolicy().hasHeightForWidth())
        self.pushButton.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Calibri")
        self.pushButton.setFont(font)
        self.pushButton.setObjectName("pushButton")
        self.gridLayout.addWidget(self.pushButton, 1, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1266, 22))
        self.menubar.setObjectName("menubar")
        self.menuTable = QtWidgets.QMenu(self.menubar)
        self.menuTable.setObjectName("menuTable")
        self.menuAnalisis = QtWidgets.QMenu(self.menubar)
        self.menuAnalisis.setObjectName("menuAnalisis")
        self.menuRport = QtWidgets.QMenu(self.menubar)
        self.menuRport.setObjectName("menuRport")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.actionCreate_final_table = QtWidgets.QAction(MainWindow)
        self.actionCreate_final_table.setObjectName("actionCreate_final_table")
        self.actionCoppy_data_from_table = QtWidgets.QAction(MainWindow)
        self.actionCoppy_data_from_table.setObjectName("actionCoppy_data_from_table")
        self.actionDelete_Query = QtWidgets.QAction(MainWindow)
        self.actionDelete_Query.setObjectName("actionDelete_Query")
        self.actionSave = QtWidgets.QAction(MainWindow)
        self.actionSave.setShortcut(QtGui.QKeySequence(Qt.CTRL + Qt.Key_S))
        self.actionSave.setObjectName("actionSave")
        self.actionExit = QtWidgets.QAction(MainWindow)
        self.actionExit.setObjectName("actionExit")
        self.actionQuic_Outlier_Detection = QtWidgets.QAction(MainWindow)
        self.actionQuic_Outlier_Detection.setObjectName("actionQuic_Outlier_Detection")
        self.actionCreate_chart = QtWidgets.QAction(MainWindow)
        self.actionCreate_chart.setObjectName("actionCreate_chart")
        self.actionReport_Wizzard = QtWidgets.QAction(MainWindow)
        self.actionReport_Wizzard.setObjectName("actionReport_Wizzard")
        self.menuTable.addAction(self.actionCreate_final_table)
        self.menuTable.addAction(self.actionCoppy_data_from_table)
        self.menuTable.addAction(self.actionDelete_Query)
        self.menuTable.addSeparator()
        self.menuTable.addAction(self.actionSave)
        self.menuTable.addAction(self.actionExit)
        self.menuTable.addSeparator()
        self.menuAnalisis.addAction(self.actionQuic_Outlier_Detection)
        self.menuAnalisis.addAction(self.actionCreate_chart)
        self.menuRport.addAction(self.actionReport_Wizzard)
        self.menubar.addAction(self.menuTable.menuAction())
        self.menubar.addAction(self.menuAnalisis.menuAction())
        self.menubar.addAction(self.menuRport.menuAction())

        self.retranslateUi(MainWindow, MW_name)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
        self.actionCreate_final_table.triggered.connect(self.Create_table)
        self.actionExit.triggered.connect(MainWindow.close)
        self.actionSave.triggered.connect(MainWindow.statusBar().showMessage('Changes saved'))
        self.actionCoppy_data_from_table.triggered.connect(self.import_data)
        self.actionCreate_chart.triggered.connect(self.create_plot)
        self.actionReport_Wizzard.triggered.connect(self.show_report_wizard)
        
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # FILL UI WITH DATA      
    def retranslateUi(self, MainWindow, MW_name):
        MainWindow.setWindowTitle(QtWidgets.QApplication.translate("MainWindow", MW_name, None, -1))
        self.pushButton.setText(QtWidgets.QApplication.translate("MainWindow", "Refresh", None, -1))
        self.menuTable.setTitle(QtWidgets.QApplication.translate("MainWindow", "Table", None, -1))
        self.menuAnalisis.setTitle(QtWidgets.QApplication.translate("MainWindow", "Analisis", None, -1))
        self.menuRport.setTitle(QtWidgets.QApplication.translate("MainWindow", "Rport", None, -1))
        self.actionCreate_final_table.setText(QtWidgets.QApplication.translate("MainWindow", "Create final table", None, -1))
        self.actionCoppy_data_from_table.setText(QtWidgets.QApplication.translate("MainWindow", "Coppy data from table", None, -1))
        self.actionDelete_Query.setText(QtWidgets.QApplication.translate("MainWindow", "Delete Query", None, -1))
        self.actionExit.setText(QtWidgets.QApplication.translate("MainWindow", "Exit", None, -1))
        self.actionQuic_Outlier_Detection.setText(QtWidgets.QApplication.translate("MainWindow", "Quic Outlier Detection", None, -1))
        self.actionCreate_chart.setText(QtWidgets.QApplication.translate("MainWindow", "Create chart", None, -1))
        self.actionReport_Wizzard.setText(QtWidgets.QApplication.translate("MainWindow", "Report Wizzard", None, -1))
        self.actionSave.setText(QtWidgets.QApplication.translate("MainWindow", "Save    Ctrl+S", None, -1))

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # SLOT FOR 'CREATE TABLE' DIALOG
    def Create_table(self):
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # CREATE INSTANCE OF DIALOG CLASS
        d = create_table_dialog(self.table_n, self.model.model.database())
        d.exec_()

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # SLOT FOR 'CREATE TABLE' DIALOG
    def import_data(self):
        import_d = CoppyTable(self.model.model.database(), self.table_n)
        import_d.exec_()
       
    def get_actionSave(self):
        print('save action triggered!!!')
        return self.actionSave
    def show_report_wizard(self):
        wiz = ReportWizard(self.table_n)
        wiz.exec_()

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # SLOT FOR 'CREATE CHART' DIALOG    
    def create_plot(self):
        pw = PlotWizard(self.table_n)
        pw.exec_()

    def contextMenuEvent(self, event):
        self.context_menu = QtWidgets.QMenu(self)
        DB_del_act = self.context_menu.addAction('Delete Row from Database')
        action = self.context_menu.exec_( self.mapToGlobal( event.pos() ) )
        if action == DB_del_act:
            self.delete_raws()

    def delete_raws(self):
        index_l = self.tableView.selectedIndexes()
        rows = set()
        for i in index_l:
            rows.add(i.row()) 
        print( rows )
        index_l = []
        for i in rows:
            index_l.append( self.filter_proxi_model.data(self.filter_proxi_model.index(i, 0), Qt.DisplayRole) )
        print( index_l )
        db = QSqlDatabase.cloneDatabase(self.model.model.database(), 'DelConn')
        db.open()
        print(db)
        query = QSqlQuery( db )
        for index in index_l:
            q = 'delete from '+ self.table+ ' where mesure_id='+str(index)+';'
            query.prepare(q)
            query.exec_()
        print(query.lastError().text() )


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# DEBUG PART
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# class MW(QMainWindow):
#     def __init__(self):
#         super(MW, self).__init__()
#         self.view = Table_Ui()
        

if __name__=='__main__':
    import sys
    app = QApplication(sys.argv)
    
    window = MW()
    window.show()

    sys.exit(app.exec_())

