# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Test_table.ui',
# licensing of 'Test_table.ui' applies.
#
# Created: Thu Jun 13 18:47:54 2019
#      by: pyside2-uic  running on PySide2 5.12.3
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1263, 886)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.frame = QtWidgets.QFrame(self.centralwidget)
        self.frame.setGeometry(QtCore.QRect(10, 10, 1231, 781))
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.tableView = QtWidgets.QTableView(self.frame)
        self.tableView.setGeometry(QtCore.QRect(-10, 10, 1231, 761))
        self.tableView.setGridStyle(QtCore.Qt.NoPen)
        self.tableView.setObjectName("tableView")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1263, 22))
        self.menubar.setObjectName("menubar")
        self.menuTable = QtWidgets.QMenu(self.menubar)
        self.menuTable.setObjectName("menuTable")
        self.menuAnalisis = QtWidgets.QMenu(self.menubar)
        self.menuAnalisis.setObjectName("menuAnalisis")
        self.menuRport = QtWidgets.QMenu(self.menubar)
        self.menuRport.setObjectName("menuRport")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.actionCreate_final_table = QtWidgets.QAction(MainWindow)
        self.actionCreate_final_table.setObjectName("actionCreate_final_table")
        self.actionCoppy_data_from_table = QtWidgets.QAction(MainWindow)
        self.actionCoppy_data_from_table.setObjectName("actionCoppy_data_from_table")
        self.actionDelete_Query = QtWidgets.QAction(MainWindow)
        self.actionDelete_Query.setObjectName("actionDelete_Query")
        self.actionExit = QtWidgets.QAction(MainWindow)
        self.actionExit.setObjectName("actionExit")
        self.actionQuic_Outlier_Detection = QtWidgets.QAction(MainWindow)
        self.actionQuic_Outlier_Detection.setObjectName("actionQuic_Outlier_Detection")
        self.actionCreate_chart = QtWidgets.QAction(MainWindow)
        self.actionCreate_chart.setObjectName("actionCreate_chart")
        self.actionReport_Wizzard = QtWidgets.QAction(MainWindow)
        self.actionReport_Wizzard.setObjectName("actionReport_Wizzard")
        self.actionSave = QtWidgets.QAction(MainWindow)
        self.actionSave.setObjectName("actionSave")
        self.menuTable.addAction(self.actionCreate_final_table)
        self.menuTable.addAction(self.actionCoppy_data_from_table)
        self.menuTable.addAction(self.actionDelete_Query)
        self.menuTable.addSeparator()
        self.menuTable.addAction(self.actionSave)
        self.menuTable.addAction(self.actionExit)
        self.menuAnalisis.addAction(self.actionQuic_Outlier_Detection)
        self.menuAnalisis.addAction(self.actionCreate_chart)
        self.menuRport.addAction(self.actionReport_Wizzard)
        self.menubar.addAction(self.menuTable.menuAction())
        self.menubar.addAction(self.menuAnalisis.menuAction())
        self.menubar.addAction(self.menuRport.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QtWidgets.QApplication.translate("MainWindow", "MainWindow", None, -1))
        self.menuTable.setTitle(QtWidgets.QApplication.translate("MainWindow", "Table", None, -1))
        self.menuAnalisis.setTitle(QtWidgets.QApplication.translate("MainWindow", "Analisis", None, -1))
        self.menuRport.setTitle(QtWidgets.QApplication.translate("MainWindow", "Rport", None, -1))
        self.actionCreate_final_table.setText(QtWidgets.QApplication.translate("MainWindow", "Create final table", None, -1))
        self.actionCoppy_data_from_table.setText(QtWidgets.QApplication.translate("MainWindow", "Coppy data from table", None, -1))
        self.actionDelete_Query.setText(QtWidgets.QApplication.translate("MainWindow", "Delete Query", None, -1))
        self.actionExit.setText(QtWidgets.QApplication.translate("MainWindow", "Exit", None, -1))
        self.actionQuic_Outlier_Detection.setText(QtWidgets.QApplication.translate("MainWindow", "Quic Outlier Detection", None, -1))
        self.actionCreate_chart.setText(QtWidgets.QApplication.translate("MainWindow", "Create chart", None, -1))
        self.actionReport_Wizzard.setText(QtWidgets.QApplication.translate("MainWindow", "Report Wizzard", None, -1))
        self.actionSave.setText(QtWidgets.QApplication.translate("MainWindow", "Save    Ctrl+S", None, -1))

