# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'oha_tree.ui',
# licensing of 'oha_tree.ui' applies.
#
# Created: Thu Jun  6 17:25:36 2019
#      by: pyside2-uic  running on PySide2 5.12.2
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1180, 758)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.OHA_frame = QtWidgets.QFrame(self.centralwidget)
        self.OHA_frame.setGeometry(QtCore.QRect(10, 40, 471, 631))
        self.OHA_frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.OHA_frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.OHA_frame.setObjectName("OHA_frame")
        self.OHA_treeView = QtWidgets.QTreeView(self.OHA_frame)
        self.OHA_treeView.setGeometry(QtCore.QRect(10, 30, 451, 591))
        self.OHA_treeView.setObjectName("OHA_treeView")
        self.treeLabe = QtWidgets.QLabel(self.OHA_frame)
        self.treeLabe.setGeometry(QtCore.QRect(10, 10, 121, 16))
        self.treeLabe.setObjectName("treeLabe")
        self.frame = QtWidgets.QFrame(self.centralwidget)
        self.frame.setGeometry(QtCore.QRect(490, 40, 661, 631))
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.tableView = QtWidgets.QTableView(self.frame)
        self.tableView.setGeometry(QtCore.QRect(10, 30, 641, 591))
        self.tableView.setObjectName("tableView")
        self.label_2 = QtWidgets.QLabel(self.frame)
        self.label_2.setGeometry(QtCore.QRect(10, 10, 91, 16))
        self.label_2.setObjectName("label_2")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1180, 22))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QtWidgets.QApplication.translate("MainWindow", "MainWindow", None, -1))
        self.treeLabe.setText(QtWidgets.QApplication.translate("MainWindow", "DATABASE NAVIGATION", None, -1))
        self.label_2.setText(QtWidgets.QApplication.translate("MainWindow", "TABLE PREVIEW", None, -1))

