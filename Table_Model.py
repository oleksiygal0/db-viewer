from PySide2.QtGui import QStandardItemModel, QStandardItem
from PySide2.QtSql import QSqlTableModel, QSqlDatabase, QSqlError, QSqlQueryModel

class Test_t_model (QSqlTableModel):
    def __init__(self, db, table):
        super(Test_t_model,self).__init__()
        self.table = table
        self.model = self.t_model(db)

    def t_model(self, db):
        conn_name = self.table+'_conn'
        try:
            model = QSqlTableModel(None, QSqlDatabase.cloneDatabase(db, conn_name))
            model.database().open()
            print( model.database().connectionName() )
        except Exception as ex:
            print(ex)
       
        try: 
            print( model.lastError() )
        except Exception as ex:
            print(ex)
        return model

    def get_t_model(self):
        return self.model

    def get_t_name(self):
        return self.table

    def applyChanges(self):
        print('save action')
        try:
            self.model.submitAll()
            '''
            IN CASE WHEN CONNECTION WITH DB IS LOST
            WE SHOULD MANUALY RESTORE IT
            '''
            if self.model.lastError().isValid():
                print(self.model.lastError())
                self.model.database().close()
                self.model.database().open()
                self.model.submitAll()
                # print(self.model.lastError())
            self.model.setTable(self.table)
            self.model.select()
        except Exception as ex:
            print(ex)



