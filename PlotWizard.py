from PySide2 import QtCore, QtGui, QtWidgets
import configparser 
import mysql.connector as sql
from pandas import DataFrame
import pandas as pd
from PlotWindow import CustomPlot
# import threading
import sys

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# COMMENT BOX 1
# REIMPLEMENT DRAG_EVENT METHODS FOR QLISTWIDGET CLASS
# FOR ANABLE PROPPER DRAG AND DROP FUNCTIONALITY
class TargetList(QtWidgets.QListWidget):
    def __init__(self, parent=None):
        super(TargetList, self).__init__(parent)
        self.setDragDropMode(QtWidgets.QAbstractItemView.DragDrop)
    def dragEnterEvent(self, e):
        e.accept()
        source = e.source()
        if 'TargetList' in str(type(source)):
            print(self.currentRow())
            item = self.takeItem(self.currentRow())
            del item
            print('item remove event')
    def dropEvent(self, e):
        if e.mimeData().hasText():
            self.addItem(e.mimeData().text())
            print('drop event')

class SourceList(QtWidgets.QListWidget):
    def __init__(self, parent=None):
        super(SourceList, self).__init__(parent)
        self.setDragDropMode(QtWidgets.QAbstractItemView.DragDrop)
    def dragEnterEvent(self, e):
        items = self.selectedItems()
        text = items[0].text()
        e.accept()
        e.mimeData().setText(text)
# END OF COMMENT BOX 1
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# COMENT BOX 2
# UI CLASS INITALIZATION
class PlotWizard(QtWidgets.QDialog):
    def __init__(self, table, parent=None):
        super(PlotWizard, self).__init__()
        self.table = table
        self.setupUi(self)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("accepted()"), self.accept_slot)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("rejected()"), self.reject)
        self.comboBox.currentIndexChanged.connect(self.comboBox_index_changed)
        self.initialise_table_fields()
        
        # hide unused controls:
        self.spinBox.setVisible(False)
        self.label_7.setVisible(False)

    def initialise_table_fields(self):
        conf = configparser.ConfigParser()
        conf.read('config.ini')
        user = conf['connection']['user']
        password = conf['connection']['password']
        host = conf['connection']['host']
        database = conf['connection']['database']
        port = conf['connection']['port']
        db_connection = sql.connect(host=host, database=database, user = user, password = password, 
            port = port, auth_plugin='mysql_native_password')
        query = 'SELECT * FROM '+self.table+';'
        self.df = pd.read_sql(query,db_connection)
        for col in self.df.columns:
            self.listWidget.addItem(col)

    def comboBox_index_changed(self):
        text = self.comboBox.currentText()
        if text == 'BoxPlot':
            self.spinBox.setVisible(False)
            self.label_7.setVisible(False)
            self.listWidget_2.setVisible(False)
            self.label_3.setVisible(False)
        elif text == 'Histogram':
            self.spinBox.setVisible(True)
            self.label_7.setVisible(True)
            self.listWidget_2.setVisible(False)
            self.label_3.setVisible(False)
        elif text == 'Pivot Plot':
            self.spinBox.setVisible(False)
            self.label_7.setVisible(False)
            self.listWidget_2.setVisible(True)
            self.label_3.setVisible(True)

    def accept_slot(self):
        '''
            plot_dict = {'Filter':['TA'], 'Rows':['VD_V_arg', 'RSET_Ohm_arg'],
                'Columns':['Chip'], 'Value':['ACL_A_res'], 'Bins':None}
        '''
        self.plot_dict = {'Filter':None, 'Rows':None, 'Columns':None, 'Value':None, 'Bins':None}
        df_filter = None
        df_rows = None
        df_columns = None
        df_value = None
        bins = None
        if self.spinBox.value()>0:
            bins = self.spinBox.value()
        # fill df_filter array
        count = self.listWidget_5.count()
        if count != 0:
            df_filter = []
            for i in range(count):
                df_filter.append( self.listWidget_5.item(i).text() )
        # fill df_rows array
        count = self.listWidget_2.count()
        if count != 0:
            df_rows = []
            for i in range(count):
                df_rows.append( self.listWidget_2.item(i).text() )
        # fill df_columns array
        count = self.listWidget_3.count()
        if count != 0:
            df_columns = []
            for i in range(count):
                df_columns.append( self.listWidget_3.item(i).text() )
        # fill df_value array
        count = self.listWidget_4.count()
        if count != 0:
            df_value = []
            for i in range(count):
                df_value.append( self.listWidget_4.item(i).text() )
        
        self.plot_dict['Filter'] =df_filter
        self.plot_dict['Rows'] = df_rows
        self.plot_dict['Columns'] = df_columns
        self.plot_dict['Value'] = df_value
        self.plot_dict['Bins'] = bins
        self.create_window()
        # print(plot_dict)
        # t = threading.Thread(target=self.create_window)
        # t.daemon = True
        # t.start()
        
        self.accept()

    def create_window(self):
        # plot_app = QtWidgets.QApplication(sys.argv)
        CustomPlot(self.df, self.plot_dict).show()
        # plot_app.exec_()

    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(619, 497)
        self.gridLayout_2 = QtWidgets.QGridLayout(Dialog)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(Dialog)
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(10)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.comboBox = QtWidgets.QComboBox(Dialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.comboBox.sizePolicy().hasHeightForWidth())
        self.comboBox.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(10)
        self.comboBox.setFont(font)
        self.comboBox.setObjectName("comboBox")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.verticalLayout.addWidget(self.comboBox)
        self.gridLayout_2.addLayout(self.verticalLayout, 0, 0, 1, 1)
        self.line = QtWidgets.QFrame(Dialog)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.gridLayout_2.addWidget(self.line, 1, 0, 1, 2)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.verticalLayout_6 = QtWidgets.QVBoxLayout()
        self.verticalLayout_6.setObjectName("verticalLayout_6")
        self.label_2 = QtWidgets.QLabel(Dialog)
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(10)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.verticalLayout_6.addWidget(self.label_2)
        self.listWidget = SourceList(Dialog)
        font = QtGui.QFont()
        font.setFamily("Calibri")
        self.listWidget.setFont(font)
        self.listWidget.setObjectName("listWidget")
        self.verticalLayout_6.addWidget(self.listWidget)
        self.horizontalLayout.addLayout(self.verticalLayout_6)
        self.line_2 = QtWidgets.QFrame(Dialog)
        self.line_2.setFrameShape(QtWidgets.QFrame.VLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.horizontalLayout.addWidget(self.line_2)
        self.gridLayout_2.addLayout(self.horizontalLayout, 2, 0, 1, 1)
        self.verticalLayout_7 = QtWidgets.QVBoxLayout()
        self.verticalLayout_7.setObjectName("verticalLayout_7")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.label_3 = QtWidgets.QLabel(Dialog)
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(10)
        self.label_3.setFont(font)
        self.label_3.setObjectName("label_3")
        self.verticalLayout_2.addWidget(self.label_3)
        self.listWidget_2 = TargetList(Dialog)
        font = QtGui.QFont()
        font.setFamily("Calibri")
        self.listWidget_2.setFont(font)
        self.listWidget_2.setObjectName("listWidget_2")
        self.verticalLayout_2.addWidget(self.listWidget_2)
        self.gridLayout.addLayout(self.verticalLayout_2, 0, 0, 1, 1)
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.label_5 = QtWidgets.QLabel(Dialog)
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(10)
        self.label_5.setFont(font)
        self.label_5.setObjectName("label_5")
        self.verticalLayout_3.addWidget(self.label_5)
        self.listWidget_5 = TargetList(Dialog)
        font = QtGui.QFont()
        font.setFamily("Calibri")
        self.listWidget_5.setFont(font)
        self.listWidget_5.setObjectName("listWidget_5")
        self.verticalLayout_3.addWidget(self.listWidget_5)
        self.gridLayout.addLayout(self.verticalLayout_3, 0, 1, 1, 1)
        self.verticalLayout_5 = QtWidgets.QVBoxLayout()
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.label_4 = QtWidgets.QLabel(Dialog)
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(10)
        self.label_4.setFont(font)
        self.label_4.setObjectName("label_4")
        self.verticalLayout_5.addWidget(self.label_4)
        self.listWidget_3 = TargetList(Dialog)
        font = QtGui.QFont()
        font.setFamily("Calibri")
        self.listWidget_3.setFont(font)
        self.listWidget_3.setObjectName("listWidget_3")
        self.verticalLayout_5.addWidget(self.listWidget_3)
        self.gridLayout.addLayout(self.verticalLayout_5, 1, 0, 1, 1)
        self.verticalLayout_4 = QtWidgets.QVBoxLayout()
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.label_6 = QtWidgets.QLabel(Dialog)
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(10)
        self.label_6.setFont(font)
        self.label_6.setObjectName("label_6")
        self.verticalLayout_4.addWidget(self.label_6)
        self.listWidget_4 = TargetList(Dialog)
        font = QtGui.QFont()
        font.setFamily("Calibri")
        self.listWidget_4.setFont(font)
        self.listWidget_4.setObjectName("listWidget_4")
        self.listWidget_4.installEventFilter(self)
        self.verticalLayout_4.addWidget(self.listWidget_4)
        self.gridLayout.addLayout(self.verticalLayout_4, 1, 1, 1, 1)
        self.verticalLayout_7.addLayout(self.gridLayout)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label_7 = QtWidgets.QLabel(Dialog)
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(10)
        self.label_7.setFont(font)
        self.label_7.setObjectName("label_7")
        self.horizontalLayout_2.addWidget(self.label_7)
        self.spinBox = QtWidgets.QSpinBox(Dialog)
        self.spinBox.setObjectName("spinBox")
        self.horizontalLayout_2.addWidget(self.spinBox)
        self.verticalLayout_7.addLayout(self.horizontalLayout_2)
        self.gridLayout_2.addLayout(self.verticalLayout_7, 2, 1, 1, 1)
        self.buttonBox = QtWidgets.QDialogButtonBox(Dialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.gridLayout_2.addWidget(self.buttonBox, 3, 1, 1, 1)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def eventFilter(self, source, event):
        
        if ( event.type() == QtCore.QEvent.ContextMenu and
            source is self.listWidget_4 ):
            menu = QtWidgets.QMenu()
            mean = menu.addAction('mean')
            median = menu.addAction('median')
            s_min = menu.addAction('min')
            s_max = menu.addAction('max')
            action = menu.exec_(event.globalPos())
            item = source.itemAt(event.pos())
            try:
                if action == menu or median or s_max or s_min:
                    item_str = item.text()
                    if '.' in item_str:
                        item_str = item_str.split('.')[1]
                    new_str = '{0}.{1}'.format(action.text(), item_str)
                    print(new_str)
                    item.setText(new_str)
            except Exception as ex:
                print(ex)
        return super(PlotWizard, self).eventFilter(source, event)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QtWidgets.QApplication.translate("Dialog", "Dialog", None, -1))
        self.label.setText(QtWidgets.QApplication.translate("Dialog", "Select Plot Type", None, -1))
        self.comboBox.setItemText(0, QtWidgets.QApplication.translate("Dialog", "Pivot Plot", None, -1))
        self.comboBox.setItemText(1, QtWidgets.QApplication.translate("Dialog", "BoxPlot", None, -1))
        # self.comboBox.setItemText(2, QtWidgets.QApplication.translate("Dialog", "Histogram", None, -1))
        self.label_2.setText(QtWidgets.QApplication.translate("Dialog", "Column List", None, -1))
        self.label_3.setText(QtWidgets.QApplication.translate("Dialog", "Raws", None, -1))
        self.label_5.setText(QtWidgets.QApplication.translate("Dialog", "Filter", None, -1))
        self.label_4.setText(QtWidgets.QApplication.translate("Dialog", "Columns", None, -1))
        self.label_6.setText(QtWidgets.QApplication.translate("Dialog", "Value", None, -1))
        self.label_7.setText(QtWidgets.QApplication.translate("Dialog", "Bins", None, -1))

if __name__=='__main__':
    import sys

    app = QtWidgets.QApplication(sys.argv)
    window = PlotWizard('slg5aph1320_sr_xd_0')
    window.show()
    sys.exit(app.exec_())