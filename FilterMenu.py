# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'filterMenu.ui',
# licensing of 'filterMenu.ui' applies.
#
# Created: Tue Jul 16 09:31:20 2019
#      by: pyside2-uic  running on PySide2 5.12.3
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets
from PySide2.QtCore import Qt

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(241, 332)
        self.buttonBox = QtWidgets.QDialogButtonBox(Dialog)
        self.buttonBox.setEnabled(True)
        self.buttonBox.setGeometry(QtCore.QRect(20, 300, 211, 32))
        self.buttonBox.setAcceptDrops(False)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setCenterButtons(False)
        self.buttonBox.setObjectName("buttonBox")
        self.checkBox = QtWidgets.QCheckBox(Dialog)
        self.checkBox.setGeometry(QtCore.QRect(10, 0, 86, 30))
        font = QtGui.QFont()
        font.setFamily("Calibri")
        self.checkBox.setFont(font)
        self.checkBox.setObjectName("checkBox")
        self.checkBox.setTristate(True)
        self.checkBox.setCheckState(Qt.Checked)
        self.listView = QtWidgets.QListView(Dialog)
        self.listView.setGeometry(QtCore.QRect(10, 30, 221, 271))
        self.listView.setObjectName("listView")

        self.retranslateUi(Dialog)
        # QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("accepted()"), Dialog.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("rejected()"), Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QtWidgets.QApplication.translate("Dialog", "Dialog", None, -1))
        self.checkBox.setText(QtWidgets.QApplication.translate("Dialog", "SelectAll", None, -1))

    def get_list(self):
        return self.listView

    def get_checkbox(self):
        return self.checkBox


from PySide2.QtWidgets import QApplication, QDialog
import sys

class mf(QDialog):
    def __init__(self):
        super(mf, self).__init__()
        self.view = Ui_Dialog()
        self.view.setupUi(self)

if __name__ == "__main__":
    app = QApplication(sys.argv)

    d = mf()
    d.exec_()
    
    sys.exit(app.exec_())