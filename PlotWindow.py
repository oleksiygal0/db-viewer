# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'PlotWindow.ui',
# licensing of 'PlotWindow.ui' applies.
#
# Created: Fri Aug  2 13:58:28 2019
#      by: pyside2-uic  running on PySide2 5.12.3
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets
# from mplwidget import MplWidget
from matplotlib.backends.backend_qt5agg import (NavigationToolbar2QT as NavigationToolbar)
import pandas as pd
from matplotlib import pyplot as plt

from matplotlib.backends.backend_qt5agg import FigureCanvas
from matplotlib.figure import Figure
import numpy as np

class CustomPlot(QtWidgets.QMainWindow):
    def __init__(self, data_frame, analisis_val):
        self.analisis_val = analisis_val
        super(CustomPlot, self).__init__()
        self.df = data_frame
        self.analisis_val = analisis_val
        self.filterDict = dict()
        self.XFilterDict = dict()
        self.setupUi(self)  #  CHECK THIS !!!!! 
        self.tool_bar = NavigationToolbar(FigureCanvas(Figure()), self)
        self.addToolBar(self.tool_bar)

        self.fig = Figure()

        self.applyButton1.clicked.connect(self.update_graph)
        self.applyButton2.clicked.connect(self.update_graph)
        self.add_filter()
        self.add_XData()
        self.update_graph() #show graph

    def setupUi(self, MainWindow):

        self.canvas = FigureCanvas(Figure())
        vertical_layout = QtWidgets.QVBoxLayout()
        vertical_layout.addWidget(self.canvas)
        self.canvas.axes = self.canvas.figure.add_subplot(111)


        self.MainWindow = MainWindow
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(853, 381)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(10)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.filterHorizonLayout = QtWidgets.QHBoxLayout()
        self.filterHorizonLayout.setObjectName("filterHorizonLayout")
        # APPLY BUTTON 2
        self.applyButton1 = QtWidgets.QPushButton(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.applyButton1.sizePolicy().hasHeightForWidth())
        self.applyButton1.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Calibri")
        self.applyButton1.setFont(font)
        self.applyButton1.setObjectName("applyButton1")
        self.applyButton1.setText('Apply')

        self.verticalLayout.addLayout(self.filterHorizonLayout)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)
        self.MplWidget = QtWidgets.QWidget(self.centralwidget)
        self.MplWidget.setLayout(vertical_layout)

        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.MinimumExpanding, QtWidgets.QSizePolicy.MinimumExpanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.MplWidget.sizePolicy().hasHeightForWidth())
        self.MplWidget.setSizePolicy(sizePolicy)
        self.MplWidget.setObjectName("MplWidget")
        self.gridLayout.addWidget(self.MplWidget, 1, 0, 1, 1)
        self.XAxeHorizonLayout = QtWidgets.QHBoxLayout()
        self.XAxeHorizonLayout.setObjectName("XAxeHorizonLayout")
        # APPLY BUTTON 2
        self.applyButton2 = QtWidgets.QPushButton(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.applyButton2.sizePolicy().hasHeightForWidth())
        self.applyButton2.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Calibri")
        self.applyButton2.setFont(font)
        self.applyButton2.setObjectName("applyButton2")
        self.applyButton2.setText('Apply')

        self.gridLayout.addLayout(self.XAxeHorizonLayout, 2, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 853, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QtWidgets.QApplication.translate("MainWindow", "MainWindow", None, -1))
        self.label.setText(QtWidgets.QApplication.translate("MainWindow", "Filters", None, -1))

    def update_graph(self):
        '''
        values_dict = {'Filter':['TA'], 'Rows':['VD_V_arg', 'RSET_Ohm_arg'],
            'Columns':['Chip'], 'Value':['ACL_A_res'], 'Bins':None}
        '''
        tmp_df = self.df
        for key, value in self.filterDict.items():
            df_type = self.df[key].dtype.name
            lisBox = value[1]
            # lisBox = QtWidgets.QListWidget()
            filter_list = []
            for i in  lisBox.selectedItems():
                if 'int' in df_type:
                    filter_list.append(int(i.text()))
                elif 'float' in df_type:
                    filter_list.append(float(i.text()))
                elif 'object' in df_type:
                    filter_list.append(i.text())
                elif 'bool' in df_type:
                    filter_list.append(bool(i.text()))
            print(filter_list)
            tmp_df = tmp_df[tmp_df[key].isin(filter_list)]
        for key, value in self.XFilterDict.items():
            df_type = self.df[key].dtype.name
            lisBox = value[1]
            # lisBox = QtWidgets.QListWidget()
            filter_list = []
            for i in  lisBox.selectedItems():
                if 'int' in df_type:
                    filter_list.append(int(i.text()))
                elif 'float' in df_type:
                    filter_list.append(float(i.text()))
                elif 'object' in df_type:
                    filter_list.append(i.text())
                elif 'bool' in df_type:
                    filter_list.append(bool(i.text()))
            print(filter_list)
            tmp_df = tmp_df[tmp_df[key].isin(filter_list)]

        self.fig.clf()
        plt.close(self.fig)
             
        # replace layout that contain matplotlib canvas in MplWidget
        # self.canvas.axes.clear()
        self.fig.clf()

        plot_type = ''
        if (self.analisis_val['Bins'] == None) and (self.analisis_val['Rows'] == None):
            # BOXPLOT
            plot_type = 'BOXPLOT'
            columns = self.analisis_val['Columns']
            values = self.analisis_val['Value']
            self.ax = tmp_df.boxplot(column=values, by = columns, rot = 90)
            
            self.fig = self.ax.get_figure()  

        elif self.analisis_val['Bins'] != None:
            # HISTOGRAM
            pass
            plot_type = 'HISTOGRAM'
            columns = self.analisis_val['Columns']
            values = self.analisis_val['Value']
            bins = self.analisis_val['Bins']
            self.ax = tmp_df.hist(column=values, by = columns, bins = bins)
            # self.fig = self.ax.get_figure() 
            print(len(self.ax))
            print(len(self.ax[0]))
        else:
            # PIVOT PLOT 
            # [np.mean, np.median, min, max]
            mapping_np_funct = {'mean':np.mean, 'median':np.median, 'min':min, 'max':max}
            plot_type = 'PIVOT'
            columns = self.analisis_val['Columns']
            values = self.analisis_val['Value']
            raws = self.analisis_val['Rows']
            numpy_funct = []

            velue_container = []
            # if values also contain modifiers we should use other pivot table
            i = 0
            for v in values:
                if '.' in v:
                    numpy_funct.append( mapping_np_funct[v.split('.')[0]] )
                    velue_container.append(v.split('.')[1]) 
                i = i + 1

            if len(numpy_funct) > 0:
                velue_container = set(velue_container)
                velue_container = list(velue_container)
                pt = tmp_df.pivot_table(values = velue_container, index = raws, aggfunc=numpy_funct)
                self.ax = pt.plot()
            else:
                pt = tmp_df.pivot_table(columns = columns, values = values, index = raws)
                self.ax = pt.plot.line()

            self.fig = self.ax.get_figure()

        # replace toolbar:
        self.removeToolBar(self.tool_bar)
        self.tool_bar = NavigationToolbar(FigureCanvas(self.fig), self)
        self.addToolBar(self.tool_bar)

        # replace widget layout
        canvas = FigureCanvas(self.fig)
        # canvas.axes = canvas.figure.add_subplot(self.ax)
        widget_from = self.MplWidget.layout().replaceWidget(self.canvas, canvas)
        self.canvas = canvas
        del widget_from

        if plot_type == 'PIVOT':
            # make plot lines hideble
            leg = self.ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), shadow=True, ncol=10) 
            # associate lines object with legend lines
            lines = self.ax.get_lines()
            self.lined = dict()
            for legline, origline in zip(leg.get_lines(), lines):
                legline.set_picker(5)  # 5 pts tolerance
                self.lined[legline] = origline
            self.fig.canvas.mpl_connect('pick_event', self.onpick)
        



    def onpick(self, event):
        # on the pick event, find the orig line corresponding to the
        # legend proxy line, and toggle the visibility
        legline = event.artist
        origline = self.lined[legline]
        vis = not origline.get_visible()
        origline.set_visible(vis)
        # Change the alpha on the line in the legend so we can see what lines
        # have been toggled
        if vis:
            legline.set_alpha(1.0)
        else:
            legline.set_alpha(0.2)
        self.fig.canvas.draw()


    '''
    THIS FUNCTION DYNAMICALLY CREATE COMBOBOX IN QT SPECIFIC LAYOUT
    self.filterHorizonLayout SHOULD BE MODIFIED
    '''
    # FIRTHT I SHOULD INITIALISE DICTIONARY THAT CONTAINS FILTER FIELDS
    def add_filter(self):
        if self.analisis_val['Filter'] == None:
            self.applyButton1.setVisible(False)
            return None
        self.applyButton1.setVisible(True)
        filters_names = self.analisis_val['Filter']
        for name in filters_names:
            label = QtWidgets.QLabel(self.MainWindow)
            font = QtGui.QFont()
            font.setFamily("Calibri")
            font.setPointSize(10)
            label.setFont(font)
            obj_name = name+'_label'
            label.setObjectName(obj_name)
            label.setText(name)

            listBox = QtWidgets.QListWidget(self.MainWindow)
            listBox.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
            sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
            sizePolicy.setHorizontalStretch(0)
            sizePolicy.setVerticalStretch(0)
            sizePolicy.setHeightForWidth(listBox.sizePolicy().hasHeightForWidth())
            listBox.setSizePolicy(sizePolicy)
            listBox.setMaximumSize(QtCore.QSize(100, 80))
            font = QtGui.QFont()
            font.setFamily("Calibri")
            font.setPointSize(10)
            listBox.setFont(font)
            obj_name = name+'_listBox'
            listBox.setObjectName(obj_name)
            for value in self.df[name].unique():
                listBox.addItem(str(value))
            listBox.selectAll()
            
            line = QtWidgets.QFrame(self.MainWindow)
            line.setFrameShape(QtWidgets.QFrame.VLine)
            line.setFrameShadow(QtWidgets.QFrame.Sunken)
            obj_name = name+'_listBox'
            line.setObjectName(obj_name)

            self.filterDict[name] = [label, listBox, line]
        for key, value in self.filterDict.items():
            for i in value:
                self.filterHorizonLayout.addWidget(i)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.filterHorizonLayout.addWidget(self.applyButton1)
        self.filterHorizonLayout.addItem(spacerItem)


    '''
    THIS FUNCTION DYNAMICALLY CREATE COMBOBOX IN QT SPECIFIC LAYOUT
    self.XAxeHorizonLayout SHOULD BE MODIFIED
    '''
    # FIRTHT I SHOULD INITIALISE DICTIONARY THAT CONTAINS FILTER FIELDS
    def add_XData(self):
        if self.analisis_val['Rows'] == None:
            self.applyButton2.setVisible(False)
            return None
        self.applyButton2.setVisible(True)
        for name in self.analisis_val['Rows']:
            label = QtWidgets.QLabel(self.MainWindow)
            font = QtGui.QFont()
            font.setFamily("Calibri")
            font.setPointSize(10)
            label.setFont(font)
            obj_name = name+'_label'
            label.setObjectName(obj_name)
            label.setText(name)

            listBox = QtWidgets.QListWidget(self.MainWindow)
            listBox.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
            
            sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
            sizePolicy.setHorizontalStretch(0)
            sizePolicy.setVerticalStretch(0)
            sizePolicy.setHeightForWidth(listBox.sizePolicy().hasHeightForWidth())
            listBox.setSizePolicy(sizePolicy)
            listBox.setMaximumSize(QtCore.QSize(100, 80))
            font = QtGui.QFont()
            font.setFamily("Calibri")
            font.setPointSize(10)
            listBox.setFont(font)
            obj_name = name+'_listBox'
            listBox.setObjectName(obj_name)
            for value in self.df[name].unique():
                listBox.addItem(str(value))
            listBox.selectAll()
            
            line = QtWidgets.QFrame(self.MainWindow)
            line.setFrameShape(QtWidgets.QFrame.VLine)
            line.setFrameShadow(QtWidgets.QFrame.Sunken)
            obj_name = name+'_listBox'
            line.setObjectName(obj_name)

            self.XFilterDict[name] = [label, listBox, line]
        for key, value in self.XFilterDict.items():
            for i in value:
                self.XAxeHorizonLayout.addWidget(i)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.XAxeHorizonLayout.addWidget(self.applyButton2)
        self.XAxeHorizonLayout.addItem(spacerItem)

if __name__ == '__main__':
    import configparser
    import sys
    import pandas as pd
    from pandas import ExcelWriter

    df = pd.read_excel('C:\\Users\\ohal\\dev\\db_view\\acl.xlsx')
    print(df.head(5))
    # mapping_np_funct = {'mean':np.mean, 'median':np.median, 'min':min, 'max':max}
    boxplot_dict = {'Filter':['TA','writer'], 'Rows':None,
                'Columns':['VD_V_arg', 'RSET_Ohm_arg'], 'Value':['ACL_A_res'], 'Bins':None}

    pivot_dict = {'Filter':['TA'], 'Rows':['VD_V_arg', 'RSET_Ohm_arg'],
                'Columns':['Chip'], 'Value':['ACL_A_res'], 'Bins':None}

    hist_dict = {'Filter':['TA','writer'], 'Rows':None,
                'Columns':['VD_V_arg', 'RSET_Ohm_arg'], 'Value':['ACL_A_res'], 'Bins':15}
    app = QtWidgets.QApplication(sys.argv)
     
    plot_window = CustomPlot(df, pivot_dict)
    plot_window.show()
    sys.exit(app.exec_())





