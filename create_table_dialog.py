from PySide2 import QtCore, QtGui, QtWidgets
from PySide2.QtWidgets import QDialogButtonBox, QApplication, QDialog
from PySide2.QtSql import QSqlDatabase, QSqlQuery, QSqlError
import sys

class Ui_CreateTable(object):
    def setupUi(self, CreateTable):
        self.CreateTable = CreateTable
        self.CreateTable.setObjectName("CreateTable")
        self.CreateTable.resize(407, 342)
        self.gridLayout = QtWidgets.QGridLayout(self.CreateTable)
        self.gridLayout.setObjectName("gridLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label_4 = QtWidgets.QLabel(self.CreateTable)
        font = QtGui.QFont()
        font.setFamily("Calibri")
        self.label_4.setFont(font)
        self.label_4.setObjectName("label_4")
        self.horizontalLayout.addWidget(self.label_4)
        spacerItem = QtWidgets.QSpacerItem(58, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.lineEdit = QtWidgets.QLineEdit(self.CreateTable)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lineEdit.sizePolicy().hasHeightForWidth())
        self.lineEdit.setSizePolicy(sizePolicy)
        self.lineEdit.setMinimumSize(QtCore.QSize(200, 0))
        font = QtGui.QFont()
        font.setFamily("Calibri")
        self.lineEdit.setFont(font)
        self.lineEdit.setContextMenuPolicy(QtCore.Qt.PreventContextMenu)
        self.lineEdit.setText("")
        self.lineEdit.setObjectName("lineEdit")
        self.horizontalLayout.addWidget(self.lineEdit)
        self.gridLayout.addLayout(self.horizontalLayout, 0, 0, 1, 1)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label_3 = QtWidgets.QLabel(self.CreateTable)
        font = QtGui.QFont()
        font.setFamily("Calibri")
        self.label_3.setFont(font)
        self.label_3.setObjectName("label_3")
        self.horizontalLayout_2.addWidget(self.label_3)
        spacerItem1 = QtWidgets.QSpacerItem(78, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem1)
        self.lineEdit_2 = QtWidgets.QLineEdit(self.CreateTable)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lineEdit_2.sizePolicy().hasHeightForWidth())
        self.lineEdit_2.setSizePolicy(sizePolicy)
        self.lineEdit_2.setMinimumSize(QtCore.QSize(200, 0))
        font = QtGui.QFont()
        font.setFamily("Calibri")
        self.lineEdit_2.setFont(font)
        self.lineEdit_2.setObjectName("lineEdit_2")
        self.horizontalLayout_2.addWidget(self.lineEdit_2)
        self.gridLayout.addLayout(self.horizontalLayout_2, 1, 0, 1, 1)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(self.CreateTable)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Calibri")
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.textEdit = QtWidgets.QTextEdit(self.CreateTable)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.textEdit.sizePolicy().hasHeightForWidth())
        self.textEdit.setSizePolicy(sizePolicy)
        self.textEdit.setMaximumSize(QtCore.QSize(16777215, 101))
        font = QtGui.QFont()
        font.setFamily("Calibri")
        self.textEdit.setFont(font)
        self.textEdit.setObjectName("textEdit")
        self.verticalLayout.addWidget(self.textEdit)
        self.gridLayout.addLayout(self.verticalLayout, 2, 0, 1, 1)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.label_2 = QtWidgets.QLabel(self.CreateTable)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_2.sizePolicy().hasHeightForWidth())
        self.label_2.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Calibri")
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.verticalLayout_2.addWidget(self.label_2)
        self.textBrowser = QtWidgets.QTextBrowser(self.CreateTable)
        self.textBrowser.setMinimumSize(QtCore.QSize(387, 0))
        self.textBrowser.setMaximumSize(QtCore.QSize(16777215, 60))
        font = QtGui.QFont()
        font.setFamily("Calibri")
        self.textBrowser.setFont(font)
        self.textBrowser.setObjectName("textBrowser")
        self.verticalLayout_2.addWidget(self.textBrowser)
        self.gridLayout.addLayout(self.verticalLayout_2, 3, 0, 1, 1)
        self.buttonBox = QtWidgets.QDialogButtonBox(self.CreateTable)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.gridLayout.addWidget(self.buttonBox, 4, 0, 1, 1)

        self.lineEdit.setText(self.test_table)
        self.lineEdit_2.setText(self.new_table)
        self.textEdit.setText(self.prepare_query())
        self.retranslateUi()

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        #CONNECT SIGNALS WITH SLOTS
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("accepted()"), self.accept_action)
        # QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("accepted()"), self.CreateTable.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("rejected()"), self.CreateTable.reject)
        QtCore.QMetaObject.connectSlotsByName(self.CreateTable)
        QtCore.QMetaObject.connectSlotsByName(self.CreateTable)
        self.lineEdit_2.editingFinished.connect(self.update_query)
        
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # FILL UI ELEMENTS WITH DATA
    def retranslateUi(self):
        self.CreateTable.setWindowTitle(QtWidgets.QApplication.translate("CreateTable", "Dialog", None, -1))
        self.label_4.setText(QtWidgets.QApplication.translate("CreateTable", "Current Table", None, -1))
        self.label_3.setText(QtWidgets.QApplication.translate("CreateTable", "Version", None, -1))
        self.label.setText(QtWidgets.QApplication.translate("CreateTable", "Query Prorotyepe (you can manualy edit query if necessary)", None, -1))
        self.label_2.setText(QtWidgets.QApplication.translate("CreateTable", "Error Massege", None, -1))

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # INSERT NEW TABLE TEXT IN LINE-EDIT BOX
    def set_Test_Table_Name(self, text):
        self.test_table = text
        self.new_table = 'final'
        
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # SET DATABASE CREDENTIAL
    def set_db(self, db):
        self.query = QSqlQuery(db)

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # BUILD QUERY
    def prepare_query(self):
        query = 'CREATE TABLE '+self.test_table+'_'+self.new_table+' LIKE '+self.test_table+';'
        return query
    def update_query(self):
        self.new_table = self.lineEdit_2.text()
        self.textEdit.setText(self.prepare_query())

    def accept_action(self):
        self.query.prepare(self.textEdit.toPlainText())
        self.query.exec_()
        if self.query.lastError().isValid():
            err = self.query.lastError().text()
            self.textBrowser.setText( err )
        else:
            table = self.test_table[::-1]
            testPlan = table[:table.find('_')]
            testPlan = testPlan[::-1]
            print(testPlan)
            table = table[table.find('_')+1:]
            revision = table[:table.find('_')]
            revision = revision[::-1]
            print(revision)
            table = table[table.find('_')+1:]
            table = table[::-1]
            table = table[table.find("_")+1:]
            print(table)
            version = self.new_table

            parentTable = self.test_table[:self.test_table.find('_')]
            q = 'insert ignore into '+parentTable+" (Test, Revision, TestPlan, Version) values ("+'\''+table+'\', '+'\''+revision+'\', '+testPlan+', '+'\''+version+'\');'
            print(q)
            self.query.prepare(q)
            self.query.exec_()
            print(self.query.lastError())

            self.CreateTable.accept()



# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# DEBUG PART
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class myDialog(QDialog):
    def __init__(self, table):
        super(myDialog, self).__init__()
        self.view = Ui_CreateTable()
        self.view.set_Test_Table_Name(table)
        self.view.setupUi(self)

if __name__ == '__main__':
    app = QApplication(sys.argv)

    d = myDialog("slg5nt1769_rds_on_zs_2")
    d.show()
    sys.exit(app.exec_())

