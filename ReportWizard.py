from PySide2 import QtCore, QtGui, QtWidgets
import configparser 
import mysql.connector as sql
from pandas import DataFrame
import pandas as pd
from PlotWindow import CustomPlot
# import threading
import sys
import clr




class Row_Model(QtWidgets.QWidget) :
    
    def __init__(self, name, parent=None):
        combo_items = ['pico', 'nano','micro','milli', 'None', 'Kilo','Mega','Giga']
        super(Row_Model, self).__init__(parent)
        self.row = QtWidgets.QHBoxLayout()
        self.lineEdit = QtWidgets.QLineEdit()
        self.lineEdit.setText(name)
        self.row.addWidget(self.lineEdit)
        self.combo = QtWidgets.QComboBox()
        for i in combo_items:
            self.combo.addItem(i)
        self.row.addWidget(self.combo)
        self.setLayout(self.row)
        self.combo.setCurrentIndex(self.combo.findData('None', QtCore.Qt.DisplayRole))
    def set_text(self, text):
        self.lineEdit.setText(text)
    def get_text(self):
        return self.lineEdit.text()
    def get_combo(self):
        self.combo.currentText()
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# COMMENT BOX 1
# REIMPLEMENT DRAG_EVENT METHODS FOR QLISTWIDGET CLASS
# FOR ANABLE PROPPER DRAG AND DROP FUNCTIONALITY

class RawDataList(QtWidgets.QListWidget):
    '''
    RawDataList different from TargetList method by the method of adding new element.
    RawDataList get string data and convert it to QListWidgetItem with "editable" flag.
    '''
    def __init__(self, parent=None):
        super(RawDataList, self).__init__(parent)
        self.setDragDropMode(QtWidgets.QAbstractItemView.DragDrop)
        self.renamed_dict = dict()
        self.non_modified_names = []
        self.modified_names = []

    def dragEnterEvent(self, e):
        e.accept()
        source = e.source()
        if 'RawDataList' in str(type(source)):
            print(self.currentRow())
            item = self.takeItem(self.currentRow())
            index = self.currentRow()
            # self.modified_names.pop(index)
            self.non_modified_names.pop(index)
            # self.renamed_dict.pop([item])
            del item
            print(self.non_modified_names)

    def dropEvent(self, e):
        if e.mimeData().hasText():
            text = e.mimeData().text()
            item = QtWidgets.QListWidgetItem()
            self.addItem(item)
            row = Row_Model(text)
            item.setSizeHint(row.minimumSizeHint())
            self.setItemWidget(item, row)
            self.non_modified_names.append(text)
            print(self.non_modified_names)

class TargetList(QtWidgets.QListWidget):
    def __init__(self, parent=None):
        super(TargetList, self).__init__(parent)
        self.setDragDropMode(QtWidgets.QAbstractItemView.DragDrop)
    def dragEnterEvent(self, e):
        e.accept()
        source = e.source()
        if 'TargetList' in str(type(source)):
            print(self.currentRow())
            item = self.takeItem(self.currentRow())
            del item
            print('item remove event')
    def dropEvent(self, e):
        if e.mimeData().hasText():
            self.addItem(e.mimeData().text())
            print('drop event')

class SourceList(QtWidgets.QListWidget):
    def __init__(self, parent=None):
        super(SourceList, self).__init__(parent)
        self.setDragDropMode(QtWidgets.QAbstractItemView.DragDrop)
    def dragEnterEvent(self, e):
        items = self.selectedItems()
        text = items[0].text()
        e.accept()
        e.mimeData().setText(text)
# END OF COMMENT BOX 1
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

class ReportWizard(QtWidgets.QDialog):
    def __init__(self, table):
        super(ReportWizard, self).__init__()
        self.table = table
        self.setupUi(self)
        self.initialise_table_fields()
        # self.tabWidget.tabBarClicked.connect(self.fill_PV_list)
        self.pushButton.clicked.connect(self.fill_PV_list)
        self.multiplier = []
    
    def initialise_table_fields(self):
        conf = configparser.ConfigParser()
        conf.read('config.ini')
        user = conf['connection']['user']
        password = conf['connection']['password']
        host = conf['connection']['host']
        database = conf['connection']['database']
        port = conf['connection']['port']
        db_connection = sql.connect(host=host, database=database, user = user, password = password, 
            port = port, auth_plugin='mysql_native_password')
        query = 'SELECT * FROM '+self.table+';'
        # dataframe selection
        self.df = pd.read_sql(query,db_connection)
        for col in self.df.columns:
            self.list_db_col.addItem(col)

    def fill_PV_list(self):
        self.multiplier.clear()
        self.list_PT_col.clear()
        for i in range(self.list_row_data.count()):
            item = self.list_row_data.item(i)
            row_widget = self.list_row_data.itemWidget(item)
            text = row_widget.get_text()
            self.list_PT_col.addItem( text )
            self.multiplier.append(row_widget.get_combo())

    def eventFilter(self, source, event):
        if ( event.type() == QtCore.QEvent.ContextMenu and
            source is self.list_value ):
            menu = QtWidgets.QMenu()
            Average = menu.addAction('Average')
            CountNums = menu.addAction('CountNums')
            Max = menu.addAction('Max')
            Min = menu.addAction('Min')
            Product = menu.addAction('Product')
            StDev = menu.addAction('StDev')
            StDevP = menu.addAction('StDevP')
            Sum = menu.addAction('Sum')
            Var = menu.addAction('Var')
            VarP = menu.addAction('VarP')

            action = menu.exec_(event.globalPos())
            item = source.itemAt(event.pos())
            try:
                if action == Average or CountNums or Max or Min or Product or StDev or StDevP or Sum or Var or VarP:
                    item_str = item.text()
                    if '.' in item_str:
                        item_str = item_str.split('.')[1]
                    new_str = '{0}.{1}'.format(action.text(), item_str)
                    print(new_str)
                    item.setText(new_str)
            except Exception as ex:
                print(ex)
        return super(ReportWizard, self).eventFilter(source, event)

    def file_select(self):
        file = QtWidgets.QFileDialog.getOpenFileName(self, 'Open file', 'Desktop')
        self.linePath.setText(file[0])

    def accept_action(self):
        initial_col = self.list_row_data.non_modified_names
        renamed_col = []
        for i  in range(self.list_PT_col.count()):
            renamed_col.append( self.list_PT_col.item(i).text() )

        # find column to drop
        column_to_drop = []
        for i in range( self.list_db_col.count() ):
            column_to_drop.append( self.list_db_col.item(i).text() )
        for i in initial_col:
            for j in column_to_drop:
                if i == j:
                    column_to_drop.remove(j)
                    break
        self.df_n = self.df.drop(column_to_drop, axis = 1)

        # reorder columns
        self.df_n = self.df_n[initial_col]

        # rename columns
        # create map dictionary:
        map_d = dict()
        for i in range( len(initial_col) ):
            map_d[initial_col[i]] = renamed_col[i]
        self.df_n = self.df_n.rename(columns = map_d, errors="raise" )

        # sort dataframe and find sortong arg
        sorting_arg = []
        for key, val in map_d.items():
            if '_arg' in key:
                sorting_arg.append(val)
        sorting_arg.insert(0, map_d['Chip'] )
        sorting_arg.insert(0, map_d['TA'] )
        self.df_n = self.df_n.sort_values(by = sorting_arg)
        # export to excel
        path = self.linePath.text()
        path = path.replace('/', '\\')
        self.df_n.to_excel(path, sheet_name='RawData', index = False)
        
        # preparation for pivot table creation
        filters = []
        filter_values = []
        raws = []
        values = []
        for i in range( self.list_filter.count() ):
            filters.append( self.list_filter.item(i).text() )
        if len(filters) > 0:
            filter_values = self.df_n[filters[0]].unique()
        for i in range( self.list_row.count() ):
            raws.append( self.list_row.item(i).text() )
        for i in range( self.list_value.count() ):
            values.append( self.list_value.item(i).text() )
        try:
            assembly_path = r"C:\\Users\\ohal\source\\repos\\PVT_DLL\\PVT_DLL\\bin\\Debug"
            sys.path.append(assembly_path)  # add dll directory to python path
            clr.AddReference("PVT_DLL") # import dll, note that ".dll" extention is missed
            from PVT_DLL import Class1   # from namespace "Calculation" import class "calculate"
            pvt = Class1()
            pvt.createTable(raws, filters, filter_values, values, path)
        except Exception as ex:
            print(ex)
        self.accept()

    def setupUi(self, ReportWizard):
        ReportWizard.setObjectName("ReportWizard")
        ReportWizard.resize(669, 527)
        self.gridLayout_5 = QtWidgets.QGridLayout(ReportWizard)
        self.gridLayout_5.setObjectName("gridLayout_5")

        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.browsButton = QtWidgets.QPushButton(ReportWizard)
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(10)
        self.browsButton.setFont(font)
        self.browsButton.setObjectName("browsButton")
        self.horizontalLayout.addWidget(self.browsButton)
        self.linePath = QtWidgets.QLineEdit(ReportWizard)
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(10)
        self.linePath.setFont(font)
        self.linePath.setObjectName("linePath")
        self.horizontalLayout.addWidget(self.linePath)
        

        self.tabWidget = QtWidgets.QTabWidget(ReportWizard)
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(10)
        self.tabWidget.setFont(font)
        self.tabWidget.setObjectName("tabWidget")
        self.tab_RowData = QtWidgets.QWidget()
        self.tab_RowData.setObjectName("tab_RowData")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.tab_RowData)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.gridLayout_2 = QtWidgets.QGridLayout()
        self.gridLayout_2.setSizeConstraint(QtWidgets.QLayout.SetMinAndMaxSize)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.label_db_col = QtWidgets.QLabel(self.tab_RowData)
        self.label_db_col.setObjectName("label_db_col")
        self.verticalLayout.addWidget(self.label_db_col)
        self.list_db_col = SourceList(self.tab_RowData)
        self.list_db_col.setMinimumSize(QtCore.QSize(0, 393))
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(10)
        self.list_db_col.setFont(font)
        self.list_db_col.setObjectName("list_db_col")
        self.verticalLayout.addWidget(self.list_db_col)
        self.gridLayout_2.addLayout(self.verticalLayout, 0, 0, 1, 1)
        self.verticalLayout_7 = QtWidgets.QVBoxLayout()
        self.verticalLayout_7.setObjectName("verticalLayout_7")
        self.label_row_data = QtWidgets.QLabel(self.tab_RowData)
        self.label_row_data.setObjectName("label_row_data")
        self.verticalLayout_7.addWidget(self.label_row_data)
        self.list_row_data = RawDataList(self.tab_RowData)
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(10)
        self.list_row_data.setFont(font)
        self.list_row_data.setObjectName("list_row_data")
        self.verticalLayout_7.addWidget(self.list_row_data)
        self.gridLayout_2.addLayout(self.verticalLayout_7, 0, 2, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_2.addItem(spacerItem, 0, 1, 1, 1)

        self.gridLayout_3.addLayout(self.gridLayout_2, 0, 0, 1, 1)
        self.verticalLayout_8 = QtWidgets.QVBoxLayout()
        self.verticalLayout_8.setObjectName("verticalLayout_8")
        spacerItem1 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_8.addItem(spacerItem1)
        self.pushButton = QtWidgets.QPushButton(self.tab_RowData)
        self.pushButton.setObjectName("pushButton")
        self.verticalLayout_8.addWidget(self.pushButton)
        self.gridLayout_3.addLayout(self.verticalLayout_8, 0, 1, 1, 1)

        self.tabWidget.addTab(self.tab_RowData, "")
        self.tab_PivotTable = QtWidgets.QWidget()
        self.tab_PivotTable.setObjectName("tab_PivotTable")
        self.gridLayout_4 = QtWidgets.QGridLayout(self.tab_PivotTable)
        self.gridLayout_4.setObjectName("gridLayout_4")
        self.verticalLayout_6 = QtWidgets.QVBoxLayout()
        self.verticalLayout_6.setObjectName("verticalLayout_6")
        self.label_PT_col = QtWidgets.QLabel(self.tab_PivotTable)
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(10)
        self.label_PT_col.setFont(font)
        self.label_PT_col.setObjectName("label_PT_col")
        self.verticalLayout_6.addWidget(self.label_PT_col)
        self.list_PT_col = SourceList(self.tab_PivotTable)
        font = QtGui.QFont()
        font.setFamily("Calibri")
        self.list_PT_col.setFont(font)
        self.list_PT_col.setObjectName("list_PT_col")
        self.verticalLayout_6.addWidget(self.list_PT_col)
        self.gridLayout_4.addLayout(self.verticalLayout_6, 0, 0, 1, 1)
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.label_filter = QtWidgets.QLabel(self.tab_PivotTable)
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(10)
        self.label_filter.setFont(font)
        self.label_filter.setObjectName("label_filter")
        self.verticalLayout_2.addWidget(self.label_filter)
        self.list_filter = TargetList(self.tab_PivotTable)
        font = QtGui.QFont()
        font.setFamily("Calibri")
        self.list_filter.setFont(font)
        self.list_filter.setObjectName("list_filter")
        self.verticalLayout_2.addWidget(self.list_filter)
        self.gridLayout.addLayout(self.verticalLayout_2, 0, 0, 1, 1)
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.label_columns = QtWidgets.QLabel(self.tab_PivotTable)
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(10)
        self.label_columns.setFont(font)
        self.label_columns.setObjectName("label_columns")
        self.verticalLayout_3.addWidget(self.label_columns)
        self.list_column = TargetList(self.tab_PivotTable)
        font = QtGui.QFont()
        font.setFamily("Calibri")
        self.list_column.setFont(font)
        self.list_column.setObjectName("list_column")
        self.verticalLayout_3.addWidget(self.list_column)
        self.gridLayout.addLayout(self.verticalLayout_3, 0, 1, 1, 1)
        self.verticalLayout_5 = QtWidgets.QVBoxLayout()
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.label_row = QtWidgets.QLabel(self.tab_PivotTable)
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(10)
        self.label_row.setFont(font)
        self.label_row.setObjectName("label_row")
        self.verticalLayout_5.addWidget(self.label_row)
        self.list_row = TargetList(self.tab_PivotTable)
        font = QtGui.QFont()
        font.setFamily("Calibri")
        self.list_row.setFont(font)
        self.list_row.setObjectName("list_row")
        self.verticalLayout_5.addWidget(self.list_row)
        self.gridLayout.addLayout(self.verticalLayout_5, 1, 0, 1, 1)
        self.verticalLayout_4 = QtWidgets.QVBoxLayout()
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.label_value = QtWidgets.QLabel(self.tab_PivotTable)
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(10)
        self.label_value.setFont(font)
        self.label_value.setObjectName("label_value")
        self.verticalLayout_4.addWidget(self.label_value)
        self.list_value = TargetList(self.tab_PivotTable)
        font = QtGui.QFont()
        font.setFamily("Calibri")
        self.list_value.setFont(font)
        self.list_value.setObjectName("list_value")
        self.list_value.installEventFilter(self)
        self.verticalLayout_4.addWidget(self.list_value)
        self.gridLayout.addLayout(self.verticalLayout_4, 1, 1, 1, 1)
        self.gridLayout_4.addLayout(self.gridLayout, 0, 1, 1, 1)
        self.tabWidget.addTab(self.tab_PivotTable, "")
        self.gridLayout_5.addLayout(self.horizontalLayout, 0, 0, 1, 1)
        self.gridLayout_5.addWidget(self.tabWidget, 1, 0, 1, 1)
        self.buttonBox = QtWidgets.QDialogButtonBox(ReportWizard)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        
        self.gridLayout_5.addWidget(self.buttonBox, 2, 0, 1, 1)

        self.retranslateUi(ReportWizard)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("accepted()"), self.accept_action)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("rejected()"), ReportWizard.reject)
        QtCore.QMetaObject.connectSlotsByName(ReportWizard)
        self.browsButton.clicked.connect(self.file_select)

    def retranslateUi(self, ReportWizard):
        
        ReportWizard.setWindowTitle(QtWidgets.QApplication.translate("ReportWizard", "Report Wizard", None, -1))
        self.browsButton.setText(QtWidgets.QApplication.translate("ReportWizard", "Brows", None, -1))
        self.label_db_col.setText(QtWidgets.QApplication.translate("ReportWizard", "Database Column", None, -1))
        self.label_row_data.setText(QtWidgets.QApplication.translate("ReportWizard", "Row Data Column", None, -1))
        self.pushButton.setText(QtWidgets.QApplication.translate("ReportWizard", "Procceed", None, -1))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_RowData), QtWidgets.QApplication.translate("ReportWizard", "RowData", None, -1))
        self.label_PT_col.setText(QtWidgets.QApplication.translate("ReportWizard", "Column List", None, -1))
        self.label_filter.setText(QtWidgets.QApplication.translate("ReportWizard", "Filters", None, -1))
        self.label_columns.setText(QtWidgets.QApplication.translate("ReportWizard", "Columns", None, -1))
        self.label_row.setText(QtWidgets.QApplication.translate("ReportWizard", "Rows", None, -1))
        self.label_value.setText(QtWidgets.QApplication.translate("ReportWizard", "Values", None, -1))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_PivotTable), QtWidgets.QApplication.translate("ReportWizard", "PivotTable", None, -1))

if __name__ == '__main__':
    import sys

    app = QtWidgets.QApplication(sys.argv)
    window = ReportWizard('slg5aph1320_acl_xd_0')
    window.show()
    sys.exit(app.exec_())
