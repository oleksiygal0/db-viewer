import sys

from PySide2.QtWidgets import QApplication, QMainWindow, QHeaderView, QMenu
from PySide2.QtCore import SIGNAL, SLOT, QObject
from PySide2.QtSql import QSqlQuery, QSqlDatabase
from oha_tree import Ui_MainWindow
from model import oha_model

from Table_View import Table_Ui
from Table_Model import Test_t_model

class MW(QMainWindow):
    def __init__(self):
        super(MW,self).__init__()
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # CONSTRACT VIEW   
        self.view = Ui_MainWindow()
        self.view.setupUi(self)
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # CONSTRACT MODEL
        self.model = oha_model()
        self.view.setTreeModel( self.model.get_tree())  # !!!!!!!
        self.view.OHA_treeView.header().setSectionResizeMode(QHeaderView.ResizeToContents)
        # self.tableView.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeToContents)
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # BIND SLOT AND SIGNAL
        self.view.getTree().clicked.connect(self.s_clic)
        self.view.getTree().doubleClicked.connect(self.d_click)

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # SINGLE CLICK SLOT
    def s_clic(self, tree): 
        try:   
            table = self.view.get_table_name()
        except:
            table = None
        if table == None:
            pass
        else:
            self.model.TablePreview(table)
            self.view.setTableModel( self.model.get_table() )

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # DOUBLE CLICK SLOT
    def d_click(self):
        table = self.view.get_table_name()
        # db_credential = self.model.get_db_credential()
        self.tw = Table_Ui(table, self.model.get_table().database())
        self.tw.show()

    def contextMenuEvent(self, event):
            contextMenu = QMenu(self)
            refreshAct = contextMenu.addAction('Refresh')
            delAction = contextMenu.addAction('Delete Table')
            action = contextMenu.exec_(self.mapToGlobal(event.pos()))
            if action == refreshAct:
                self.refresh_tree()
            elif action == delAction:
                self.delete_table()

    def refresh_tree(self):
        self.model.refresh_tree()
        self.view.setTreeModel( self.model.get_tree())

    def delete_table(self):
        index_list = self.view.OHA_treeView.selectedIndexes()
        table = ''
        field_values = []
        for index in index_list:
            field_values.append( self.model.tree.data(index) )
            if len(self.model.tree.data(index))>0:
                table = table+'_'+self.model.tree.data(index)
        
        parent_index = self.model.tree.parent(index_list[0]) 
        test_name = self.model.tree.data(parent_index)
        table = test_name + table
        print(table)
        qdelete_t = 'drop table if exists {0};'.format(table)
        qdelete_f = 'delete from {0} where Test=\'{1}\' and Revision=\'{2}\' and TestPlan={3} and Version=\'{4}\';'.format(
            test_name, field_values[0], field_values[1],field_values[2],field_values[3])
        print(qdelete_t)
        print(qdelete_f)
        db = QSqlDatabase.cloneDatabase(self.model.table.database(), 'DelConn1')
        db.open()
        query = QSqlQuery(db)
        query.exec_(qdelete_t)
        print(query.lastError())
        query.exec_(qdelete_f)
        print(query.lastError())

def main():
    from PySide2.QtWidgets import QApplication
    app = QApplication(sys.argv)
    
    window = MW()
    window.show()

    sys.exit(app.exec_())

if __name__ == "__main__":
    main()